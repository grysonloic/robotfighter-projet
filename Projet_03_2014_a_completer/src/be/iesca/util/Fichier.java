package be.iesca.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.nio.file.Files;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import be.iesca.domaine.Bundle;

/**
 * Classe permettant de manipuler des fichiers.
 * 
 * @author O.Legrand
 * @version 2.
 * 
 */
public class Fichier {

	/**
	 * Copie un fichier d'un r�pertoire vers un autre r�pertoire
	 * 
	 * @param nomFichier
	 *            : fichier � copier
	 * @param repSource
	 *            : r�pertoire contenant le fichier � copier
	 * @param repDestination
	 *            : repertoire du nouveau fichier cr��
	 */
	public static void copierFichier(String nomFichier, String repSource,
			String repDestination) {
		
		File fileSource = new File(repSource, nomFichier);
		File fileDest = new File(repDestination, nomFichier);
		try {
			Files.copy(fileSource.toPath(), fileDest.toPath());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Supprime tous les fichiers d'un r�pertoire. Ne supprime pas le r�pertoire
	 * lui-m�me.
	 * 
	 * @param repertoire
	 *            : repertoire dont les fichiers seront supprim�s
	 * @throws IOException
	 *             : exception si probl�me d'acc�s aux fichiers, au r�pertoire.
	 */
	public static void supprimerFichiers(String repertoire) throws IOException {
		File fileRepertoire = new File(repertoire);
		if (fileRepertoire == null || !fileRepertoire.exists())
			throw new IOException(
					"Suppression impossible,  repertoire inexistant");
		File[] fichiers = fileRepertoire.listFiles();
		if (fichiers == null)
			return;
		for (File f : fichiers) {
			f.delete();
		}
	}

	/**
	 * Supprime le fichier sp�cifi�. Il doit �tre pr�sent dans le r�pertoire.
	 * 
	 * @param nomFichier
	 *            : nom du fichier � supprimer
	 * @param repertoire
	 *            : r�pertoire contenant le fichier � supprimer.
	 * @throws IOException
	 *             : exception si probl�me d'acc�s au fichier, au r�pertoire.
	 */
	public static void supprimerUnFichier(String nomFichier, String repertoire)
			throws IOException {
		File file = new File(repertoire, nomFichier);
		if (file == null || !file.exists())
			throw new IOException("Suppression impossible,  fichier inexistant");
		file.delete();
	}

	/**
	 * M�thode permettant de lire un fichier de caract�res (ascii) et de le
	 * sauver en DB. Les caract�res du fichier sont plac�s dans le param�tre
	 * sp�cifi� du prepareStatement. Cette m�thode doit �tre appel�e � partir
	 * d'un DAO qui persiste en DB.
	 * 
	 * @param repertoire
	 *            : repertoire source contenant le fichier.
	 * @param nomFichier
	 *            : nom du fichier dont les caract�res vont �tre lus.
	 * @param ps
	 *            : prepareStatement dont un param�tre va �tre garni avec les
	 *            caract�res du fichier.
	 * @param numParam
	 *            : num�ro du param�tre du prepareStatement � garnir.
	 * @throws Exception
	 *             : si probl�me IO, SQL, ...
	 */
	public static void lectureEtprepareStatement(String repertoire,
			String nomFichier, PreparedStatement ps, int numParam)
			throws Exception {
		File file = new File(repertoire, nomFichier);
		FileInputStream fis = new FileInputStream(file);
		InputStreamReader isr = new InputStreamReader(fis, "UTF8");
		ps.setCharacterStream(numParam, isr, (int) file.length());
	}

	/**
	 * M�thode permettant de cr�er un fichier contenant des caract�res stock�s
	 * en DB. Cr�ation d'un fichier contenant les caract�res contenu dans la
	 * colonne dont le nom est sp�cifi�. La colonne est obtenue suite � un
	 * query. Cette m�thode doit �tre appel�e � partir d'un DAO qui persiste en
	 * DB.
	 * Remarque : si le fichier existe, il sera remplac� par le nouveau.
	 * 
	 * @param resultSet
	 *            : resultat du query.
	 *            ex: "SELECT nom, codesource FROM Robots r WHERE r.nom = ?"
	 * @param nomColonne
	 *            : nom de la colonne de la table contenant les caract�res �
	 *            �crire dans le fichier. ex: "codesource"
	 * @param repertoire
	 *            : r�pertoire du fichier qui va �tre cr��.
	 * @param nomFichier
	 *            : nom du fichier cr��. ex: "Crazy.java"
	 * @throws Exception
	 *             : lanc�e si probl�me IO, SQL, ...
	 */
	public static void creerFichier(ResultSet resultSet, String nomColonne,
			String repertoire, String nomFichier) throws Exception {
		Reader reader = resultSet.getCharacterStream(nomColonne);
		File file = new File(Bundle.REPERTOIRE_ROBOTS + nomFichier);
		try {
			FileOutputStream fos = new FileOutputStream(file);
			Writer out = new OutputStreamWriter(fos, "UTF8");
			int car;
			while ((car = reader.read()) > -1) {
				out.write(car);
			}
			out.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}
}