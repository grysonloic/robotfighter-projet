package be.iesca.dao;

import java.util.List;

import be.iesca.domaine.Bundle;

import be.iesca.domaine.User;

public interface BatailleDao extends Dao{

	List<String> listerBataille(User user);
	int getBataille( Bundle bundle);
	boolean supprimerRobotBataille(String id, Bundle bundle);
	boolean creerBataille(Bundle bundle, User user);
	boolean ajouterRobotBataille(String id,Bundle bundle);
	boolean supprimerBataille(Bundle bundle);

}
