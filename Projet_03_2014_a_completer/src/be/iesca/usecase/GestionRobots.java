package be.iesca.usecase;

import be.iesca.domaine.Bundle;

public interface GestionRobots {
	
	void ajouterRobot(Bundle bundle);
	void supprimerRobot (Bundle bundle);
	void vider(Bundle bundle);
	void listerRobot(Bundle bundle);
	void listerRobotAnciennesBatailles(Bundle bundle);
	void lancementBataille(Bundle bundle);
	

}
