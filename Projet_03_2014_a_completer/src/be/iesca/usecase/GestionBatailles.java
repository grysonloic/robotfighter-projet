package be.iesca.usecase;

import be.iesca.domaine.Bundle;

public interface GestionBatailles {
	
	void listerBatailles(Bundle bundle);
	void getBataille(Bundle bundle);
	void supprimerRobotBataille(Bundle bundle);
	boolean creerBataille(Bundle bundle);
	
	
}
