package be.iesca.ihm;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import be.iesca.controleur.GestionnaireUseCases;
import be.iesca.domaine.Bundle;
import be.iesca.domaine.ClasseRobot;

public class AjoutRobotAncBataille  extends JDialog  {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@SuppressWarnings("unused")
	private MenuBataille vcp;
	private GestionnaireUseCases gestionnaire =  GestionnaireUseCases.getInstance();
	private JButton jbAjouter = new JButton("Ajouter robot");
	private JButton jbRetour = new JButton("Annuler");
	@SuppressWarnings("unused")
	private JLabel JlTitre = new JLabel("Robots", JLabel.CENTER);

	@SuppressWarnings("unused")
	private JComboBox<String> jcbNom = new JComboBox<String>();
	@SuppressWarnings("unused")
	private List<String> listeNom = new ArrayList<String>();
	private Bundle bundle = new Bundle();
	private ListerRobot lr = null;
	@SuppressWarnings("unused")
	private JScrollPane jsp = new JScrollPane();
	public AjoutRobotAncBataille (MenuBataille vcp)
	{
		super(vcp);
		this.setModal(true);
		this.getRootPane().setDefaultButton(jbAjouter);
		jbAjouter.setToolTipText("Ajouter robot(s) � la bataille");
		jbAjouter.setMnemonic(KeyEvent.VK_A);
		jbRetour.setToolTipText("retour menu ancienne bataille");
		
		
		
		this.vcp=vcp;
		this.bundle = vcp.getBundle();
		this.setPreferredSize(new Dimension(500,300));
		this.setResizable(false);
		
		this.setLayout(new BorderLayout());
		
		lr = new ListerRobot(bundle,3);
		this.add(jsp = new JScrollPane(lr), BorderLayout.CENTER);
		
		this.add(creerPanelBouton(), BorderLayout.SOUTH);
		this.ajouterListenerBoutons();
		
		this.pack();
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		this.setLocationRelativeTo(vcp);
		this.setVisible(true);
	}
	
	private JPanel creerPanelBouton() {
		JPanel JpBoutons = new JPanel();
		JpBoutons.add(this.jbAjouter);
		JpBoutons.add(this.jbRetour);

		return JpBoutons;
	}
	private void annuler() {
		
		this.dispose();

	}@SuppressWarnings("unused")
	protected void ajouterRobotBataille(Bundle bundle)
	{
		boolean creationReussie=true;
		int i=0;
		List<String> listCheck = lr.getListId();
		List<String> listNom = lr.getList();

			if (listCheck.isEmpty())
				JOptionPane.showMessageDialog(this, "Vous n'avez pas s�lectionner de robots � ajouter", "Identification",
						JOptionPane.ERROR_MESSAGE);
			else  
			{
				
				this.gestionnaire.getBataille(bundle);
				
					for (String lc : listCheck) 
					{
						ClasseRobot robot = new ClasseRobot();
						robot.setId(lc);
						robot.setNom(listNom.get(i));
	
						bundle.put(Bundle.ROBOT, robot);
						gestionnaire.ajouterRobotBataille(bundle);
						i++;
					}
					
					String message = (String) bundle.get(Bundle.MESSAGE);
					
					if ((boolean)this.bundle.get(Bundle.OPERATION_REUSSIE) == true)
					{
						JOptionPane.showMessageDialog(this, message, "Identification",
								JOptionPane.INFORMATION_MESSAGE);
						
					}
					else JOptionPane.showMessageDialog(this, message, "Identification",
							JOptionPane.ERROR_MESSAGE);
				}
						
			}
				
			
		

	private void ajouterListenerBoutons() {
		this.jbAjouter.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				ajouterRobotBataille(bundle);
			}
		});
		this.jbRetour.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				annuler();
			}

		});
	}
	

}
