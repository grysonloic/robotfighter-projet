package be.iesca.ihm;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import be.iesca.controleur.GestionnaireUseCases;
import be.iesca.domaine.Bundle;
import be.iesca.domaine.ClasseRobot;

public class MenuRobot extends JDialog {

	private static final long serialVersionUID = 1L;
	private GestionnaireUseCases gestionnaire = GestionnaireUseCases
			.getInstance();
	private JLabel JlTitre = new JLabel("Robots", JLabel.CENTER);
	private JButton JbNewRobot = new JButton("Nouveau");
	private JButton JbVidRobot = new JButton("Vider");
	private JButton JbSupRobot = new JButton("Supprimer");
	private JButton JbAnnuler = new JButton("Annuler");
	private JDialog fc;
	private Bundle bundle = new Bundle();
	private ListerRobot lr;
	private JScrollPane jsp;
	private VueControleurPrincipal vcp;

	public MenuRobot(VueControleurPrincipal vcp) {
		super(vcp.getJFrame(), true);
		
		this.vcp = vcp;
		this.bundle = vcp.getBundle();
		this.setPreferredSize(new Dimension(400, 300));
		this.setResizable(false);

		// bulles + gestion clavier boutons
		this.JbNewRobot.setToolTipText("Ajouter votre propore robot (ALT+N");
		this.JbNewRobot.setMnemonic(KeyEvent.VK_N);

		this.JbVidRobot.setToolTipText("Supprimer tous vos robots (ALT+V");
		this.JbVidRobot.setMnemonic(KeyEvent.VK_V);

		this.JbSupRobot
				.setToolTipText("Supprimer la selection de robots ALT+S");
		this.JbSupRobot.setMnemonic(KeyEvent.VK_S);

		this.JbAnnuler.setToolTipText("Retour au menu principal");

		this.setLayout(new BorderLayout());
		this.add(JlTitre, BorderLayout.NORTH);

		lr = new ListerRobot(bundle,1);
		this.add(jsp = new JScrollPane(lr), BorderLayout.CENTER);

		this.add(creerPanelBouton(), BorderLayout.SOUTH);
		this.ajouterListenerBoutons();

		this.pack();
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		this.setLocationRelativeTo(vcp.getJFrame());
		this.setVisible(true);
	}

	private JPanel creerPanelBouton() {
		JPanel JpBoutons = new JPanel();
		JpBoutons.add(this.JbNewRobot);
		JpBoutons.add(this.JbVidRobot);
		JpBoutons.add(this.JbSupRobot);
		JpBoutons.add(this.JbAnnuler);

		return JpBoutons;
	}

	private void annuler() {
		this.setVisible(false);

	}

	private void supprimerRobot() {
		int i=0;
		List<String> listCheck = lr.getListId();
		List<String> listNom = lr.getList();
		
		
			if (listCheck.isEmpty())
				JOptionPane.showMessageDialog(this, "Vous n'avez pas s�lectionner de robots � supprimer", "Identification",
						JOptionPane.ERROR_MESSAGE);
			
			else  {
					int ok =JOptionPane.showConfirmDialog(this, "Voulez-vous supprimer le(s) robot(s) selection�(s) ?", "",JOptionPane.ERROR_MESSAGE);
					if (ok == JOptionPane.OK_OPTION)
					{
					
						for (String lc : listCheck) {
							ClasseRobot robot = new ClasseRobot();
							robot.setId(lc);
							robot.setNom(listNom.get(i));
							//String path="./src/robots";
							//robot.supprimerRobot(robot.getNom(), path);
							bundle.put(Bundle.ROBOT, robot);
							gestionnaire.supprimerRobot(bundle);
							i++;
						}
						
						String message = (String) bundle.get(Bundle.MESSAGE);
						
						if ((boolean)this.bundle.get(Bundle.OPERATION_REUSSIE) == true)
						{
							JOptionPane.showMessageDialog(this, message, "Identification",
									JOptionPane.INFORMATION_MESSAGE);
							
						}
						else JOptionPane.showMessageDialog(this, message, "Identification",
								JOptionPane.ERROR_MESSAGE);
						
						this.remove(jsp);
						this.lr.removeAll();
						this.lr = new ListerRobot(bundle,1);
						this.add(jsp = new JScrollPane(lr), BorderLayout.CENTER);
						this.revalidate();
					
					}
				}
	}

	private void viderRobot() {
		
		JOptionPane confirm = new JOptionPane();
		@SuppressWarnings("static-access")
		int ok =confirm.showConfirmDialog(this, "Voulez-vous supprimer tous les robots ?", "",
				JOptionPane.ERROR_MESSAGE);
		
		if(ok == JOptionPane.OK_OPTION){

			gestionnaire.vider(bundle);
			this.remove(jsp);
			this.lr.removeAll();
			this.lr = new ListerRobot(bundle ,1 );
			this.add(jsp = new JScrollPane(lr), BorderLayout.CENTER);
			this.revalidate();
		
		
		}
		
	}
	
	private void NewRobot(){
		this.dispose();
		new NouveauRobot(fc,this, vcp );
	}

	private void ajouterListenerBoutons() {
		this.JbNewRobot.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				NewRobot();
			}
		});

		this.JbVidRobot.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				viderRobot();
			}
		});

		this.JbSupRobot.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				supprimerRobot();
			}
		});

		this.JbAnnuler.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				annuler();
			}

		});
	}
	
	
	public Bundle getBundle() {

		return bundle;
	}

}
