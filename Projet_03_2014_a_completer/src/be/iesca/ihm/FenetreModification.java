package be.iesca.ihm;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import be.iesca.controleur.GestionnaireUseCases;
import be.iesca.domaine.Bundle;
import be.iesca.domaine.User;


public class FenetreModification extends JDialog{

	private static final long serialVersionUID = 1L;
	private JLabel JlTitre = new JLabel("Modifier Compte", JLabel.CENTER);
	
	private JTextField jtfPseudo = new JTextField(20);
	private JTextField jtfEmail = new JTextField(20);
	private JPasswordField jPassword = new JPasswordField(20);
	
	private JButton jbModifier = new JButton("Modifier");
	private JButton jbAnnuler = new JButton("Annuler");
	
	private GestionnaireUseCases gestionnaire;
	
	private User user = new User();
	private Bundle bundle = new Bundle();
	
	public FenetreModification(JFrame frame, Bundle bundle)
	{
		super();
		
		this.getRootPane().setDefaultButton(jbModifier);
		this.jbModifier.setToolTipText("Modifier le mot de passe");
		this.jbAnnuler.setToolTipText("Retour au menu principal");
		
		this.gestionnaire = GestionnaireUseCases.getInstance();
		this.setBundle(bundle);
		
		JlTitre.setFont(new Font("Arial", Font.BOLD, 18));
		this.add(JlTitre, BorderLayout.NORTH);
		
		this.user = (User) bundle.get(Bundle.USER);
		
		this.ajouterPanels();
		
		this.setTitle("Modification");
		this.pack();
		this.setVisible(true);
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		this.setLocationRelativeTo(frame);
		
	}
	private void modifier() {
		

	    int rep = JOptionPane.showConfirmDialog(this, "Souhaitez-vous vraiment modifier votre mot de passe ?", "Identification",
				JOptionPane.ERROR_MESSAGE);
		
	    if (rep == JOptionPane.OK_OPTION)
	    {
			String pass = new String (jPassword.getPassword());
			
			user.setNom(jtfPseudo.getText());
			user.setPassword(pass);
			user.setEmail(jtfEmail.getText());
			
			this.bundle.put(Bundle.USER, user);
			
			
			gestionnaire.modifierUser(this.bundle);
			
			String message = (String) bundle.get(Bundle.MESSAGE);
	
			if ((boolean)this.bundle.get(Bundle.OPERATION_REUSSIE) == true)
			{
				JOptionPane.showMessageDialog(this, message, "Identification",
						JOptionPane.INFORMATION_MESSAGE);
				this.setVisible(false);
			}
			else JOptionPane.showMessageDialog(this, message, "Identification",
					JOptionPane.ERROR_MESSAGE);
			
	    }
		
	}
	
	private void annuler()
	{
		this.setVisible(false);
	}
	
	private void ajouterListenersBoutons() {
		
		jbModifier.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				modifier();
			}
		});
		
		jbAnnuler.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				annuler();
			}
		});
		
	}
	
	private void ajouterPanels() {
		JPanel jpSaisies = creerPanelSaisies();
		JPanel jpBoutons = creerPanelBouton();
		this.add(jpSaisies, BorderLayout.NORTH);
		this.add(jpBoutons, BorderLayout.CENTER);
	}
	
	private JPanel creerPanelSaisies() {
		
		JPanel jpPs = new JPanel(new BorderLayout());
		jpPs.add(new JLabel("  Pseudo  "), BorderLayout.WEST);
		jpPs.add(this.jtfPseudo);
		jtfPseudo.setEditable(false);
		
		JPanel jpMail = new JPanel(new BorderLayout());
		jpMail.add(new JLabel("  Email   "), BorderLayout.WEST);
		jpMail.add(this.jtfEmail);
		jtfEmail.setEditable(false);

		JPanel jpType = new JPanel(new BorderLayout());
		jpType.add(new JLabel("  Password   "), BorderLayout.WEST);
		jpType.add(this.jPassword);

		// panel saisie
		JPanel jpSaisies = new JPanel(new GridLayout(3, 2, 30, 2));
		jpSaisies.setBorder(new TitledBorder(""));
		
		this.jtfPseudo.setText(user.getNom());
		this.jtfEmail.setText(user.getEmail());
		this.jPassword.setText("");
		
		jpSaisies.add(jpPs);
		jpSaisies.add(jpMail);
		jpSaisies.add(jpType);
		return jpSaisies;
	}

	private JPanel creerPanelBouton() {
		JPanel jpBouton = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		jpBouton.setBorder(new TitledBorder(""));
		jpBouton.add(jbModifier);
		jpBouton.add(jbAnnuler);
		ajouterListenersBoutons();
		return jpBouton;
	}
	
	private void setBundle(Bundle bundle)
	{
		this.bundle = bundle;
	}
	
	public Bundle getBundle()
	{
		return this.bundle;
	}
}
