package be.iesca.ihm;

/**
 *  Vue - Fenetre bataille
 */
import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;

import be.iesca.domaine.Bundle;

@SuppressWarnings("serial")
public class MenuBataille extends JDialog {

	private JLabel JlTitre = new JLabel("Batailles", JLabel.CENTER);

	private JDialog fc;

	private JButton jbCreer = new JButton("Cr�er");

	private JButton jbAncienne = new JButton("Anciennes batailles");
	private JButton jbSupprimerBat = new JButton("Supprimer bataille");
	private JButton jbRetour = new JButton("Retour");

	private Bundle bundle = new Bundle();
	@SuppressWarnings("unused")
	private VueControleurPrincipal vcp;

	public MenuBataille(VueControleurPrincipal vcp) {
		super(vcp.getJFrame(), true);

		this.vcp = vcp;
		this.bundle = vcp.getBundle();
		// this.setPreferredSize(new Dimension(300,300));
		this.setResizable(false);

		// bulle + clavier bouttons
		this.jbCreer.setToolTipText("Cr�er une bataille (ALT+C)");
		this.jbCreer.setMnemonic(KeyEvent.VK_C);

		this.jbAncienne
				.setToolTipText("Acc�der � des anciennes batailles (ALT+A)");
		this.jbAncienne.setMnemonic(KeyEvent.VK_A);

		this.jbSupprimerBat.setToolTipText("Supprimer une bataille (ALT+S)");
		this.jbSupprimerBat.setMnemonic(KeyEvent.VK_S);

		this.jbRetour.setToolTipText("Retourner � la page d'accueil (ALT+R)");
		this.jbRetour.setMnemonic(KeyEvent.VK_R);

		// ajouts des panels
		this.setLayout(new BorderLayout());
		this.add(creerPanelBoutonsBataille(), BorderLayout.CENTER);
		ajouterListenersBoutons();

		this.pack();
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		this.setLocationRelativeTo(vcp.getJFrame());
		this.setVisible(true);

	}

	private JPanel creerPanelBoutonsBataille() {
		this.JlTitre.setFont(new Font("Arial", Font.BOLD, 18));
		this.add(JlTitre, BorderLayout.NORTH);

		JPanel jpBoutons = new JPanel();

		jpBoutons.setLayout(new GridLayout(2, 2, 70, 30));

		jpBoutons.add(jbCreer);
		jpBoutons.add(jbAncienne);
		jpBoutons.add(jbSupprimerBat);
		jpBoutons.add(jbRetour);

		return jpBoutons;
	}

	private void menuAnciennesBatailles() {
		this.dispose();
		new FenetreAnciennesBatailles(this);

	}

	private void ajouterListenersBoutons() {
		this.jbRetour.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				retour();
			}

		});

		this.jbCreer.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				newBataille();
			}
		});

		this.jbAncienne.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {

				menuAnciennesBatailles();
			}
		});

		this.jbSupprimerBat.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				menuSupprimerBataille();
			}
		});
	}

	public Bundle getBundle() {

		return bundle;
	}

	private void newBataille() {
		this.dispose();
		new NouvelleBataille(fc, this);
	}

	private void menuSupprimerBataille() {
		this.dispose();
		new FenetreSuppressionBataille(this);
	}

	private void retour() {
		this.dispose();
	}
}
