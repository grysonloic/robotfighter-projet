package be.iesca.ihm;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import be.iesca.controleur.GestionnaireUseCases;
import be.iesca.domaine.Bundle;
import be.iesca.domaine.User;

public class FenetreInscription extends JDialog implements ActionListener {

	/**
	 * 
	 */
	// declaration de tous les field et labels
	private static final long serialVersionUID = 1L;
	private JLabel JlTitre = new JLabel("Inscription", JLabel.CENTER);
	private JLabel JlPseudo = new JLabel("Pseudo: ", JLabel.RIGHT);
	private JLabel JlEmail = new JLabel("E-mail: ", JLabel.RIGHT);
	private JLabel JlMdp = new JLabel("Mot de passe: ", JLabel.RIGHT);
	private JTextField JtPseudo = new JTextField(15);
	private JTextField JtEmail = new JTextField(15);
	private JPasswordField JpMdp = new JPasswordField(15);
	private JButton JbValider = new JButton("Valider");
	private JButton JbAnnuler = new JButton("Annuler");
	
	private Bundle bundle= new Bundle();
	private GestionnaireUseCases gestionnaire =  GestionnaireUseCases.getInstance();

	public FenetreInscription(JDialog FC) throws HeadlessException {
		// titre fenetre + operation lors de la fermeture
		//super("Inscription");
		
		super(FC);
		this.setModal(true);
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		
		//gestion touches clavier sur boutons
		this.getRootPane().setDefaultButton(JbValider);
		this.JbValider.setToolTipText("Validez votre inscription");
		this.JbAnnuler.setToolTipText("Annulez votre inscripton ");

		// ajout listener sur objets
		JbValider.addActionListener(this);
		JbAnnuler.addActionListener(this);

		// Placement du titre au dessus centre de la fenetre
		JlTitre.setFont(new Font("Arial", Font.BOLD, 18));
		this.add(JlTitre, BorderLayout.NORTH);

		// Placement et alignement des champs et labels
		JPanel jpCentre = new JPanel(new GridLayout(0, 2, 5, 2));
		JPanel jp1 = new JPanel();
		jp1.add(JlPseudo);
		jpCentre.add(jp1);
		JPanel jp2 = new JPanel();
		jp2.add(JtPseudo);
		jpCentre.add(jp2);
		JPanel jp3 = new JPanel();
		jp3.add(JlEmail);
		jpCentre.add(jp3);
		JPanel jp4 = new JPanel();
		jp4.add(JtEmail);
		jpCentre.add(jp4);
		JPanel jp5 = new JPanel();
		jp5.add(JlMdp);
		jpCentre.add(jp5);
		JPanel jp6 = new JPanel();
		jp6.add(JpMdp);
		jpCentre.add(jp6);

		// ajout label et champs au panel principal
		this.add(jpCentre);

		// ajout des boutons au panel principal
		JPanel JpSouth = new JPanel(new FlowLayout());
		JpSouth.add(JbValider);
		JpSouth.add(JbAnnuler);
		this.add(JpSouth, BorderLayout.SOUTH);

		// taille fenetre + localisation + visibilit�
		this.pack();
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		
		this.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		//System.out.println(e.getSource());
		if (e.getSource() == JbAnnuler) {
			this.dispose();
		}

		if (e.getSource() == JbValider) {
			
			inscription();
			
		}

	}
	
	
	@SuppressWarnings("static-access")
	protected void inscription() {
		
		User user = new User();
		
		user.setEmail(JtEmail.getText());
		user.setNom(JtPseudo.getText());
		user.setPassword(new String(JpMdp.getPassword()));
		
		bundle.put(Bundle.USER, user);
		
		
		this.gestionnaire.inscrireUser(bundle);
		
		if ((boolean) bundle.get(Bundle.OPERATION_REUSSIE) == true) {
			bundle.put(Bundle.MESSAGE, "Vous avez bien �t� inscrit");
			JOptionPane confirm = new JOptionPane();
			confirm.showMessageDialog(null, bundle.get(Bundle.MESSAGE),"Information", JOptionPane.INFORMATION_MESSAGE);
			this.setModal(false);
			this.dispose();
			
		} else if ((boolean) bundle.get(Bundle.OPERATION_REUSSIE) == false) {
			JOptionPane.showMessageDialog(this,
					(String) bundle.get(Bundle.MESSAGE), "Information",
					JOptionPane.ERROR_MESSAGE);
					JtEmail.setText(null);
					JtPseudo.setText(null);
					JpMdp.setText(null);
		}
		
		
	}
	
	
	public Bundle getBundle(){
		
		return bundle;
	}

}
