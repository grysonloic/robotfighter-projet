package be.iesca.ihm;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import be.iesca.controleur.GestionnaireUseCases;
import be.iesca.domaine.Bundle;
import be.iesca.domaine.ClasseRobot;

public class ListerRobot extends JPanel implements ActionListener{

	private static final long serialVersionUID = 1L;
	
	private GestionnaireUseCases gestionnaire =  GestionnaireUseCases.getInstance();
	private Bundle bundle = new Bundle();
	List<JCheckBox> listCheck;
	List<String> listId = new ArrayList<String>();
	List<String> listNom = new ArrayList<String>();
	
	public ListerRobot(Bundle bundle , int choix)
	{
		this.bundle=bundle;
		
		this.setBorder(new TitledBorder("Liste Robots"));
		switch(choix)
		{
			case 1 : this.gestionnaire.listerRobot(this.bundle);break;
			case 2 : this.gestionnaire.listerRobotAnciennesBatailles(this.bundle);break;
			case 3 : this.gestionnaire.listerRobotBataille(this.bundle);
					this.setBorder(new TitledBorder("Choix du robots"));break;
		}
		
		this.setLayout(new GridLayout(0,3,0,-5));
		
		if ((Boolean)bundle.get(Bundle.OPERATION_REUSSIE))
		{
			@SuppressWarnings("unchecked")
			List<ClasseRobot> listeRobot =(List<ClasseRobot>) bundle.get(Bundle.LISTE);
			listCheck = new ArrayList<JCheckBox>();
			
			
			
			for(ClasseRobot r : listeRobot)
			{
				JCheckBox jcb = new JCheckBox(r.getNom());
				jcb.setActionCommand(r.getId());
				jcb.addActionListener(this);
				listCheck.add(jcb);
				this.add(jcb);
				
			}
		}
		else
		{
			this.add(new JLabel((String)bundle.get(Bundle.MESSAGE)));
		}
		
		
	}
	
	public List<String> getList()
	{
		return this.listNom;
	}
	
	public List<String> getListId()
	{
		return this.listId;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		 if (((JCheckBox)e.getSource()).isSelected())
		   {
			  listId.add(((JCheckBox)e.getSource()).getActionCommand());
			  listNom.add(((JCheckBox)e.getSource()).getText());
		   }
		   else
		   {
			   listId.remove(((JCheckBox)e.getSource()).getActionCommand());
			   listNom.remove(((JCheckBox)e.getSource()).getText());
		   }
		
	}
	
	
	 	
}
	 
