package be.iesca.ihm;

import java.awt.BorderLayout;

import javax.swing.JFrame;

/**
 * Fen�tre principale de l'application
 */
@SuppressWarnings("serial")
public class FenetrePrincipale extends JFrame {
	public FenetrePrincipale() {
		super("RobotFighter");
		Model model = new Model("Veuillez vous connecter.");
		FenetreConnexion fenetreConnexion = new FenetreConnexion(this, model);
		VueMessage vueMessage = new VueMessage(model);
		this.add(fenetreConnexion,BorderLayout.CENTER);
		this.add(vueMessage, BorderLayout.SOUTH);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		this.pack();
		this.setResizable(false);
		this.setLocationRelativeTo(null);
	}
}
