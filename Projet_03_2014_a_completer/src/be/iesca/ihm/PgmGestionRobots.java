package be.iesca.ihm;

import javax.swing.SwingUtilities;

/**
 * Programme de gestion de robots
 */
public class PgmGestionRobots {
	public static void main(String[] args) {
		/*	Swing n'est pas une API "thread safe" et ne doit �tre manipul�e
		 *  que depuis un seul et unique thread, l'EDT.(Event Dispatch Thread)
		 *  Ceci permet d'�viter les "blocages" de l'application. 
		 *  http://gfx.developpez.com/tutoriel/java/swing/swing-threading/
		 */
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				FenetrePrincipale fp = new FenetrePrincipale();
				fp.setVisible(true);
			}
		});
	}
}
