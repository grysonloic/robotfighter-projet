package be.iesca.ihm;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import be.iesca.controleur.GestionnaireUseCases;
import be.iesca.domaine.Bundle;

public class FenetreSuppressionBataille extends JDialog implements ItemListener, ActionListener
{
	
	// D�claration des variables
	private static final long serialVersionUID = 1L;
	private JLabel JlTitre = new JLabel("Suppression bataille", JLabel.CENTER);
	private JButton jbSupprimer = new JButton("Supprimer");
	private JButton jbAnnuler = new JButton("Annuler");
	private Bundle bundle = new Bundle();
	private JLabel jlNom= new JLabel("Nom");
	private GestionnaireUseCases gestionnaire =  GestionnaireUseCases.getInstance();
	private JComboBox<String> jcbNom = new JComboBox<String>();
	private List<String> listeNom = new ArrayList<String>();
	private MenuBataille vcp;
	@SuppressWarnings("unused")
	private ListerRobot lr = null;
	@SuppressWarnings("unused")
	private JScrollPane jsp = new JScrollPane();
	JPanel jpNord = new JPanel();
	
	public FenetreSuppressionBataille(MenuBataille vcp)
	{
		this.vcp=vcp;
		this.bundle = vcp.getBundle();
		
		this.setLayout(new BorderLayout());
		
		jbSupprimer.addActionListener(this);
		jbAnnuler.addActionListener(this);
		
		jbAnnuler.setToolTipText("Retour menu bataille (ALT+A)");
		jbAnnuler.setMnemonic(KeyEvent.VK_A);
		
		jbSupprimer.setToolTipText("Supprime la bataille selectionn�e");
		jbSupprimer.setMnemonic(KeyEvent.VK_S);
		
		JPanel jpSud = new JPanel();
		jpSud.add(jbSupprimer);
		jpSud.add(jbAnnuler);
		
		this.add(jpSud,BorderLayout.SOUTH);

		this.add(creerPanelNord(), BorderLayout.NORTH);
		
		this.setModal(true);
		this.pack();
		this.setLocationRelativeTo(this);
		this.setVisible(true);
	}
	
	@SuppressWarnings({ "unchecked", "static-access" })
	private JPanel creerPanelNord()
	{

		jpNord.setLayout(new BorderLayout());
		jpNord.add(JlTitre, BorderLayout.NORTH);
		jpNord.add(jlNom ,BorderLayout.WEST);
		this.gestionnaire.listerBatailles(bundle);
		jcbNom.addItemListener(this);
		jcbNom.setPreferredSize(new Dimension(100, 20));
		if((boolean) bundle.get(bundle.OPERATION_REUSSIE))
		{
			listeNom =(List<String>) bundle.get(Bundle.LISTEBATAILLE);
			for(String r : listeNom)
			{
				jcbNom.addItem(r);
			}
		}
		else
			jcbNom.addItem("Aucune Bataille");
		
		jpNord.add(jcbNom, BorderLayout.CENTER);
		
		return jpNord;
	}

	@Override
	public void itemStateChanged(ItemEvent e) 
	{
    	if((boolean) bundle.get(Bundle.OPERATION_REUSSIE))
		{
	    	this.bundle.put(Bundle.BATAILLE, e.getItem());
	    	this.gestionnaire.getBataille(this.bundle);
		}
		
	}

	@Override
	public void actionPerformed(ActionEvent e) 
	{
		if(e.getSource()==jbAnnuler)
		{
			annuler();
		}
		else
			if(e.getSource()==jbSupprimer)
			{
				supprimerBataille(bundle);
			}
		
	}
	
	private void supprimerBataille(Bundle bundle) 
	{
		gestionnaire.getBataille(bundle);	
		int ok = JOptionPane.showConfirmDialog(this,"Voulez-vous supprimer cette bataille?", "Information",
				JOptionPane.YES_NO_OPTION);
		if(ok == JOptionPane.YES_OPTION)
		{
			gestionnaire.supprimerBataille(bundle);
		
			String message = (String) bundle.get(Bundle.MESSAGE);
			if ((boolean)this.bundle.get(Bundle.OPERATION_REUSSIE) == true)
			{
				JOptionPane.showMessageDialog(this, message, "Identification",
						JOptionPane.INFORMATION_MESSAGE);
				
			}
			else JOptionPane.showMessageDialog(this, message, "Identification",
					JOptionPane.ERROR_MESSAGE);
			
			// R�initialisation de la fen�tre
			this.jcbNom = new JComboBox<String>();
			this.remove(jpNord);
			this.jpNord = new JPanel();
			this.add(creerPanelNord());
			this.revalidate();		
		}
		
	}

	private void annuler() 
	{
		this.dispose();
		this.vcp.setVisible(true);
	}
	
}
