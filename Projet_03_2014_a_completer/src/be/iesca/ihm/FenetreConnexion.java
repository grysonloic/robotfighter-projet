package be.iesca.ihm;

/**
 * Fen�tre de dialogue permettant � l'utilisateur de se connecter
 */
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import be.iesca.controleur.GestionnaireUseCases;
import be.iesca.domaine.Bundle;
import be.iesca.domaine.User;

@SuppressWarnings("serial")
public class FenetreConnexion extends JPanel implements ChangeListener
{
	private JTextField jtfEmail = new JTextField(20);
	private JPasswordField jPassword = new JPasswordField(20);
	private JButton jbConnecter = new JButton("Connecter");
	private JButton jbInscrire = new JButton("Inscription");
	private GestionnaireUseCases gestionnaire;
	private Bundle bundle;
	private Model model;
	private JDialog fc;
	private JFrame jframe;
	

	public FenetreConnexion(JFrame jframe, Model model) 
	{
		this.setLayout(new BorderLayout());
		
		this.gestionnaire = GestionnaireUseCases.getInstance();
		ajouterPanels();
		this.jframe = jframe;
	    this.jframe.getRootPane().setDefaultButton(jbConnecter); //a enter ok reagit
	    this.jbConnecter.setToolTipText("Connectez-vous");
	    this.jbInscrire.setToolTipText("Inscrivez-vous (ALT+i");
	    this.jbInscrire.setMnemonic(KeyEvent.VK_I);
		this.jbInscrire.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if(e.getSource() == jbInscrire){
					new FenetreInscription(fc);
					
				}
				
			}
		});

		this.jbConnecter.addActionListener(new ActionListener() 
		{
			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				connecter();
			}
		});
		
		if (model != null) {
			this.model = model;
			this.model.addChangeListener(this);
		}

	// ------------- facilite les tests (� supprimer par la suite) -----
		this.jtfEmail.setText("toto@gmail.com");
		this.jPassword.setText("1234");
	// -----------------------------------------------------------------

	}

	private JPanel creerPanelSaisies() 
	{
		// saisie nom
		JPanel jpNom = new JPanel(new BorderLayout());
		jpNom.add(new JLabel("  Email   "), BorderLayout.WEST);
		jpNom.add(this.jtfEmail);

		// saisie type
		JPanel jpType = new JPanel(new BorderLayout());
		jpType.add(new JLabel("  Password   "), BorderLayout.WEST);
		jpType.add(this.jPassword);

		// panel saisie
		JPanel jpSaisies = new JPanel(new GridLayout(2, 1));
		jpSaisies.setBorder(new TitledBorder(""));
		jpSaisies.setPreferredSize(new Dimension(300, 75));
		jpSaisies.add(jpNom);
		jpSaisies.add(jpType);
		return jpSaisies;
	}

	private JPanel creerPanelBouton() 
	{
		JPanel jpBouton = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		jpBouton.setBorder(new TitledBorder(""));
		jpBouton.add(jbInscrire);
		jpBouton.add(jbConnecter);
		return jpBouton;
	}

	/*
	 * permet de construire les panels contenant les jtextfields "email",
	 * "password" et le bouton "connecter". Il doivent �tre ajout�s au container
	 * de la fen�tre: this.add(...).
	 */
	private void ajouterPanels() 
	{
		JPanel jpSaisies = creerPanelSaisies();
		JPanel jpBoutons = creerPanelBouton();
		this.add(jpSaisies, BorderLayout.NORTH);
		this.add(jpBoutons, BorderLayout.CENTER);
	}

	/*
	 * demande au syst�me de valider l�identification sur bases des donn�es
	 * encod�es.
	 */
	protected void connecter() 
	{
		User user = new User();
		user.setEmail(this.jtfEmail.getText());
		user.setPassword(new String(jPassword.getPassword()));
		this.bundle = new Bundle();
		bundle.put(Bundle.USER, user);
		this.gestionnaire.connecterUser(bundle);
		if ((boolean) bundle.get(Bundle.OPERATION_REUSSIE))
		{
			this.model.setBundle(bundle);
			this.jframe.remove(this);
			this.jframe.add(new VueControleurPrincipal(this.model, this.jframe, bundle), BorderLayout.CENTER);
			this.jframe.pack();
		} 
		else 
		{
			this.model.setBundle(bundle);
			String message = (String) bundle.get(Bundle.MESSAGE);
			JOptionPane.showMessageDialog(this, message, "Identification",
					JOptionPane.ERROR_MESSAGE);
		}
		
	}

	/*
	 * permet � l'application principale de savoir si la connexion a fonctionn�
	 * et d'obtenir le message g�n�r� par le syst�me.
	 */
	public Bundle getBundle() 
	{
		return bundle;
	}

	@Override
	public void stateChanged(ChangeEvent e) {
			}
}
