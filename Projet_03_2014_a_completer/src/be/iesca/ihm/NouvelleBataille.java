package be.iesca.ihm;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import be.iesca.controleur.GestionnaireUseCases;
import be.iesca.domaine.Bundle;
import be.iesca.domaine.ClasseRobot;
import be.iesca.util.Fichier;

public class NouvelleBataille extends JDialog implements ActionListener
{

	// D�claration des variables
	private static final long serialVersionUID = 1L;
	private boolean pretAlancer = false;
	private JPanel jpCentre;
	private JLabel jlTitre = new JLabel("Cr�er une bataille", JLabel.CENTER);
	
	private JLabel jlNomBat = new JLabel("Nom de la bataille: ");
	
	private JTextField jtfNomBat = new JTextField(30);
	
	private ListerRobot lr;
	private JScrollPane jsp;
	
	private JButton jbCreer = new JButton("Cr�er");
	private JButton jbLancer = new JButton("Lancer");
	private JButton jbAnnuler = new JButton("Annuler");
	
	private MenuBataille vcp ;
	private Bundle bundle = new Bundle();
	private GestionnaireUseCases gestionnaire = GestionnaireUseCases
			.getInstance();
	
	public NouvelleBataille(JDialog fc, MenuBataille vcp)
	{
		super(fc,"Cr�er une bataille");
		this.vcp = vcp;
		this.bundle = vcp.getBundle();
		
		this.setLayout(new BorderLayout());
		
		this.setModal(true);
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		
		// On ajoute les listener aux boutons
		jbCreer.addActionListener(this);
		jbLancer.addActionListener(this);
		
		jbLancer.setToolTipText("Lancer la bataille");
		jbLancer.setMnemonic(KeyEvent.VK_L);
		jbCreer.setToolTipText("Enregistrer la bataille");
		jbCreer.setMnemonic(KeyEvent.VK_C);
		jbAnnuler.setToolTipText("Retour vers la fen�tre bataille");
		jbAnnuler.addActionListener(this);

		jlTitre.setFont(new Font("Arial", Font.BOLD, 18));
		this.add(jlTitre, BorderLayout.NORTH);
		
		// Cr�ation des panel "interm�diaire"
		jpCentre = new JPanel();
		JPanel jpNomBat = new JPanel();
		// On change leur mise en page
		jpNomBat.setLayout(new GridLayout(1,2));
		jpCentre.setLayout(new BorderLayout());
		// On ajoute leur(s) composant(s)
		jpNomBat.add(jlNomBat);
		jpNomBat.add(jtfNomBat);
		jpCentre.add(jpNomBat, BorderLayout.NORTH);
		//jpCentre.add(jlChoixRobot);
		
		lr = new ListerRobot(bundle,3);
		jpCentre.add(jsp = new JScrollPane(lr), BorderLayout.CENTER);
		
		JPanel jpSud = new JPanel();
		jpSud.add(jbCreer);
		jpSud.add(jbLancer);
		jpSud.add(jbAnnuler);
		
		this.add(jpCentre);
		this.add(jpSud, BorderLayout.SOUTH);
		
		this.pack();
		this.setLocationRelativeTo(this);
		this.setVisible(true);
	}
	
	@SuppressWarnings("static-access")
	@Override
	public void actionPerformed(ActionEvent e) 
	{
		if(e.getSource()==jbAnnuler)
		{
			retour();
		}
		else
			if(e.getSource()==jbCreer)
			{
				if(jtfNomBat.getText().isEmpty())
				{
					JOptionPane.showMessageDialog(this,"Champs incomplets","Erreur",JOptionPane.ERROR_MESSAGE);
				}
				else
				{
					bundle.put(Bundle.BATAILLE, jtfNomBat.getText());
					ajouterBataille(bundle);
					reinitialisation();
				}
			}
			else
				if(e.getSource()==jbLancer)
				{
					
					if(jtfNomBat.getText().isEmpty())
					{
						// Si le champs du nom de la bataille est vide on lui affiche un msg
						JOptionPane.showMessageDialog(this,"Champs incomplets","Erreur",JOptionPane.ERROR_MESSAGE);
					}
					else
					{
						bundle.put(Bundle.BATAILLE, jtfNomBat.getText());
						ajouterBataille(bundle);
						reinitialisation();
						/* Si on ne met pas cette variable il va lancer la bataille qui a 
						 * d�j� �t� cr��e et qui porte le m�me nom que celle qu'on a voulu
						 * cr�er. On met cette variable � true quand il a ajout� les robots
						 * � la bataille */
						if(pretAlancer==true)
						{
							Fichier fich= new Fichier();
							String sourcePath =System.getProperty("user.dir") + "/robots/test-robots/";
							try {
								fich.supprimerFichiers(sourcePath);
							} catch (IOException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
							
							lancer();
							// Pour que la bataille apparaisse en premier plan
							this.setModal(false);
						}
					}
				}
	}
	
	private void retour() 
	{
		this.dispose();
		this.vcp.setVisible(true);
	}
	
	protected void ajouterBataille(Bundle bundle)
	{
		boolean creationReussie=true;
		int i=0;
		List<String> listCheck = lr.getListId();
		List<String> listNom = lr.getList();

			if (listCheck.isEmpty())
			{
				JOptionPane.showMessageDialog(this, "Vous n'avez pas s�lectionner de robots � ajouter", "Identification",
						JOptionPane.ERROR_MESSAGE);
				reinitialisation();
			}
			else 
				if(listCheck.size() < 2)
				{
					JOptionPane.showMessageDialog(this, "Vous n'avez pas s�lectionner assez de robots � ajouter", "Identification",
							JOptionPane.ERROR_MESSAGE);
					reinitialisation();
				}
			else  
			{
				creationReussie = this.gestionnaire.creerBataille(bundle);
				this.gestionnaire.getBataille(bundle);
				
				if(creationReussie == false)
				{
					JOptionPane.showMessageDialog(this,"Nom de bataille d�j� existant", "Erreur",
							JOptionPane.ERROR_MESSAGE);
					reinitialisation();
				}
				else
				{
					for (String lc : listCheck) 
					{
						ClasseRobot robot = new ClasseRobot();
						robot.setId(lc);
						robot.setNom(listNom.get(i));
	
						bundle.put(Bundle.ROBOT, robot);
						gestionnaire.ajouterRobotBataille(bundle);
						i++;
					}
					
					String message = (String) bundle.get(Bundle.MESSAGE);
					
					if ((boolean)this.bundle.get(Bundle.OPERATION_REUSSIE) == true)
					{
						JOptionPane.showMessageDialog(this, message, "Identification",
								JOptionPane.INFORMATION_MESSAGE);
						pretAlancer = true;						
					}
					else JOptionPane.showMessageDialog(this, message, "Identification",
							JOptionPane.ERROR_MESSAGE);
				}			
			}	
	}
	
	@SuppressWarnings({ "unchecked", "unused" })
	private void lancer()
	{
		int i=0;
		String sourcePath =System.getProperty("user.dir") + "/robots/test-robots/";
		
		this.gestionnaire.listerRobotAnciennesBatailles(this.bundle);
		List<ClasseRobot> listeRobot =(List<ClasseRobot>) bundle.get(Bundle.LISTE);
		// a changer
		for(ClasseRobot r : listeRobot)
		{
			i++;
		}
		
		String[] nomRobots = new String[i] ;
		i=0;
		
		for(ClasseRobot r : listeRobot)
		{
			bundle.put(Bundle.ROBOT, r);
			this.gestionnaire.lancementBataille(bundle);
			nomRobots[i]=(String) bundle.get(Bundle.NOMROBOTJAVA);
			
			i++;	
		}
		
		if(i>=2)
		{
			LanceBataille lb =  new LanceBataille(nomRobots,sourcePath) ;
			// lancement du thread
			 lb.start() ;
		}
		else 
			JOptionPane.showMessageDialog(this, "Vous n'avez pas assez de robot", "Identification",
					JOptionPane.ERROR_MESSAGE);
	}
	// Fonction utilis�e pour r�initialiser la fen�tre en cas d'erreur
	private void reinitialisation()
	{
		jtfNomBat.setText("");
		jpCentre.remove(jsp);
		this.lr.removeAll();
		this.lr = new ListerRobot(bundle,3);
		jpCentre.add(jsp = new JScrollPane(lr), BorderLayout.CENTER);
		this.add(jpCentre);
		this.revalidate();
	}
}
