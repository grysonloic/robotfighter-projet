package be.iesca.ihm;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import be.iesca.controleur.GestionnaireUseCases;
import be.iesca.domaine.Bundle;
import be.iesca.domaine.ClasseRobot;

public class NouveauRobot extends JDialog implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// declaration des variables de la fenetre
	private JLabel jlTitre = new JLabel("Nouveau Robot", JLabel.CENTER);
	private JLabel jlTexte = new JLabel("Veuillez selectionner votre fichier: ");

	private JTextField jtfRobot = new JTextField(15);

	private JButton jbParcourir = new JButton("Parcourir");
	private JButton jbOk = new JButton("Accepter");
	private JButton jbAnnuler = new JButton("Annuler");

	private Bundle bundle = new Bundle();
	private GestionnaireUseCases gestionnaire = GestionnaireUseCases
			.getInstance();
	private File fich;
	private VueControleurPrincipal vcp ;

	public NouveauRobot(JDialog fc, MenuRobot MR, VueControleurPrincipal vcp) throws HeadlessException {
		super(fc, "Nouveau Robot");
		this.vcp = vcp;
		this.bundle = MR.getBundle();
		this.setModal(true);
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);

		this.getRootPane().setDefaultButton(jbOk);
		this.jbAnnuler.setToolTipText("Retour au menu robots");
		this.jbParcourir
				.setToolTipText("Parcourir les fichiers de l'ordinateur");
		this.jbParcourir.setMnemonic(KeyEvent.VK_P);

		// activation listener
		jbParcourir.addActionListener(this);
		jbOk.addActionListener(this);
		jbAnnuler.addActionListener(this);

		// ajout des composants

		jlTitre.setFont(new Font("Arial", Font.BOLD, 18));
		this.add(jlTitre, BorderLayout.NORTH);

		JPanel jpCentre = new JPanel();
		jpCentre.add(jlTexte);
		jpCentre.add(jtfRobot);
		jpCentre.add(jbParcourir);
		this.add(jpCentre);

		JPanel jpSud = new JPanel();
		jpSud.add(jbOk);
		jpSud.add(jbAnnuler);
		this.add(jpSud, BorderLayout.SOUTH);

		this.pack();
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}

	@SuppressWarnings("static-access")
	public File choixFichier() throws Exception {
		JFileChooser file = new JFileChooser("./robots/robots");
		FileFilter filter = new FileNameExtensionFilter("Java Files", "java");
		file.addChoosableFileFilter(filter);
		file.setFileFilter(filter);
		file.showOpenDialog(this);
		file.setVisible(true);

		File fich = file.getSelectedFile();

		try {
			jtfRobot.setText(fich.getName());
		} catch (NullPointerException e) {
		}
		if (file.APPROVE_OPTION == 0) {
			return fich;
		}else
			return null;

		

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if (e.getSource() == jbAnnuler) {
			this.dispose();
			new MenuRobot(this.vcp);
		}
		if (e.getSource() == jbOk) {
			try {
				ajoutRobot(fich);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

		}
		if (e.getSource() == jbParcourir) {
			try {
				this.fich = choixFichier();
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

		}
	}

	@SuppressWarnings("static-access")
	protected void ajoutRobot(File fich) throws Exception {

		if (fich == null) {
			
			JOptionPane.showMessageDialog(rootPane, "Mauvais fichier");

		} else {
			
			ClasseRobot robot = new ClasseRobot(fich.getName(),
					fich.getPath());
			String nom = fich.getName();
		    nom=nom.replace(".java", "");
			robot.setNom(nom);
			bundle.put(bundle.ROBOT, robot);
			this.gestionnaire.ajouterRobot(bundle);
			JOptionPane.showMessageDialog(rootPane, bundle.get(Bundle.MESSAGE));
			this.dispose();
			new MenuRobot(this.vcp);
			

		}

	}

	public Bundle getBundle() {

		return bundle;
	}

}
