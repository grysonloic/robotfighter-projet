//Demoulin Sébastien
package be.iesca.ihm;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import be.iesca.controleur.GestionnaireUseCases;
import be.iesca.domaine.Bundle;
import be.iesca.domaine.ClasseRobot;
import be.iesca.util.Fichier;

public class FenetreAnciennesBatailles extends JDialog implements ItemListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private MenuBataille vcp;
	private GestionnaireUseCases gestionnaire = GestionnaireUseCases
			.getInstance();
	private JButton jbAjouter = new JButton("Ajouter robot");
	private JButton jbEnlever = new JButton(" Enlever robot(s) sélectionné(s)");
	private JButton jbLancer = new JButton("Lancer");
	private JButton jbRetour = new JButton("Annuler");
	private JLabel JlTitre = new JLabel("Anciennes Batailles", JLabel.CENTER);
	private JLabel jlNom = new JLabel("Nom");
	private JComboBox<String> jcbNom = new JComboBox<String>();
	private List<String> listeNom = new ArrayList<String>();
	private Bundle bundle = new Bundle();
	private ListerRobot lr = null;
	private JScrollPane jsp = new JScrollPane();

	public FenetreAnciennesBatailles(MenuBataille vcp) {
		super(vcp);
		this.setModal(true);
		this.vcp = vcp;
		this.bundle = vcp.getBundle();
		this.setPreferredSize(new Dimension(500, 300));
		this.setResizable(false);
		
		jbAjouter.setMnemonic(KeyEvent.VK_A);
		jbAjouter.setToolTipText("Ajouter des robots supplémentaires a la bataille");
		jbEnlever.setMnemonic(KeyEvent.VK_E);
		jbEnlever.setToolTipText("Supprimer robot(s) de la bataille");
		jbLancer.setMnemonic(KeyEvent.VK_L);
		jbLancer.setToolTipText("Lancer la bataille selectionnée");
		
		jbRetour.setToolTipText("Retour menu batailles");

		this.setLayout(new BorderLayout());
		this.add(creerPanelNord(), BorderLayout.NORTH);

		/* lister robot et lister nom */
		if ((boolean) bundle.get(Bundle.OPERATION_REUSSIE)) {
			lr = new ListerRobot(bundle, 2);
			this.add(jsp = new JScrollPane(lr), BorderLayout.CENTER);
		}

		this.add(creerPanelBouton(), BorderLayout.SOUTH);
		this.ajouterListenerBoutons();

		this.pack();
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		this.setLocationRelativeTo(vcp);
		
		this.setVisible(true);

	}

	private JPanel creerPanelBouton() {
		JPanel JpBoutons = new JPanel();
		JpBoutons.add(this.jbAjouter);
		JpBoutons.add(this.jbEnlever);
		JpBoutons.add(this.jbLancer);
		JpBoutons.add(this.jbRetour);

		return JpBoutons;
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	private JPanel creerPanelNord() {
		JPanel jpNord = new JPanel();
		// jcbNom.setBounds(new Rectangle(19,36,129,23));
		jpNord.setLayout(new BorderLayout());
		jpNord.add(JlTitre, BorderLayout.NORTH);
		jpNord.add(jlNom, BorderLayout.WEST);
		this.gestionnaire.listerBatailles(bundle);
		jcbNom.addItemListener(this);
		jcbNom.setPreferredSize(new Dimension(100, 20));
		if ((boolean) bundle.get(bundle.OPERATION_REUSSIE)) {
			listeNom = (List<String>) bundle.get(Bundle.LISTEBATAILLE);
			for (String r : listeNom) {
				jcbNom.addItem(r);
			}
		} else
			jcbNom.addItem("Aucune Bataille");
		jpNord.add(jcbNom, BorderLayout.CENTER);

		return jpNord;
	}

	private void annuler() {

		this.dispose();
		this.vcp.setVisible(true);

	}

	private void supprimerRobotBataille() {
		int i = 0;
		List<String> listCheck = lr.getListId();
		List<String> listNom = lr.getList();

		if (listCheck.isEmpty())
			JOptionPane.showMessageDialog(this,
					"Vous n'avez pas sélectionner de robots à supprimer",
					"Identification", JOptionPane.ERROR_MESSAGE);

		else {
			int ok = JOptionPane.showConfirmDialog(this,
					"Voulez-vous supprimer le(s) robot(s) selectioné(s) ?", "",
					JOptionPane.ERROR_MESSAGE);
			if (ok == JOptionPane.OK_OPTION) {

				for (String lc : listCheck) {
					ClasseRobot robot = new ClasseRobot();
					robot.setId(lc);
					robot.setNom(listNom.get(i));
					// String path="./src/robots";
					// robot.supprimerRobot(robot.getNom(), path);
					bundle.put(Bundle.ROBOT, robot);
					gestionnaire.supprimerRobotBataille(bundle);
					i++;
				}

				String message = (String) bundle.get(Bundle.MESSAGE);

				if ((boolean) this.bundle.get(Bundle.OPERATION_REUSSIE) == true) {
					JOptionPane.showMessageDialog(this, message,
							"Identification", JOptionPane.INFORMATION_MESSAGE);

				} else
					JOptionPane.showMessageDialog(this, message,
							"Identification", JOptionPane.ERROR_MESSAGE);

				this.remove(jsp);
				this.lr.removeAll();
				this.lr = new ListerRobot(bundle, 2);
				this.add(jsp = new JScrollPane(lr), BorderLayout.CENTER);
				this.revalidate();

			}
		}

	}

	private void actualisation() {
		this.remove(jsp);
		this.lr.removeAll();
		this.lr = new ListerRobot(bundle, 2);
		this.add(jsp = new JScrollPane(lr), BorderLayout.CENTER);
		this.revalidate();
	}

	private void lancer() {
		if(bundle.get(Bundle.BATAILLE)==null)
		{
			JOptionPane.showMessageDialog(this,
					"Vous n'avez pas  de bataille", "Identification",
					JOptionPane.ERROR_MESSAGE);
		}
		else
		{
		this.setModal(false);
		int i = 0;
		String sourcePath = System.getProperty("user.dir")
				+ "/robots/test-robots/";

		this.gestionnaire.listerRobotAnciennesBatailles(this.bundle);
		@SuppressWarnings("unchecked")
		List<ClasseRobot> listeRobot = (List<ClasseRobot>) bundle
				.get(Bundle.LISTE);
		// a changer
		for (@SuppressWarnings("unused")
		ClasseRobot r : listeRobot) {
			i++;
		}

		String[] nomRobots = new String[i];
		i = 0;

		for (ClasseRobot r : listeRobot) {
			bundle.put(Bundle.ROBOT, r);
			this.gestionnaire.lancementBataille(bundle);
			nomRobots[i] = (String) bundle.get(Bundle.NOMROBOTJAVA);

			i++;
		}

		if (i >= 2) {
			
			LanceBataille lb = new LanceBataille(nomRobots, sourcePath);
			// lancement du thread
			lb.start();
		} else
			JOptionPane.showMessageDialog(this,
					"Vous n'avez pas assez de robot", "Identification",
					JOptionPane.ERROR_MESSAGE);
		}
		
	}

	private void ajouterListenerBoutons() {
		this.jbAjouter.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				new AjoutRobotAncBataille(vcp);
				actualisation();

			}
		});

		this.jbEnlever.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				supprimerRobotBataille();
			}
		});

		this.jbLancer.addActionListener(new ActionListener() {
			@SuppressWarnings("static-access")
			@Override
			public void actionPerformed(ActionEvent arg0) {
				Fichier fich = new Fichier();
				String sourcePath = System.getProperty("user.dir")
						+ "/robots/test-robots/";
				try {
					fich.supprimerFichiers(sourcePath);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				lancer();
			

			}
		});

		this.jbRetour.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				annuler();
			}

		});
	}

	public void itemStateChanged(ItemEvent e) {
		if ((boolean) bundle.get(Bundle.OPERATION_REUSSIE)) {
			this.bundle.put(Bundle.BATAILLE, e.getItem());
			this.gestionnaire.getBataille(this.bundle);
		}

		this.remove(jsp);
		if (this.lr != null)
			this.lr.removeAll();
		this.lr = new ListerRobot(bundle, 2);
		this.add(jsp = new JScrollPane(lr), BorderLayout.CENTER);
		this.revalidate();
	}

}
