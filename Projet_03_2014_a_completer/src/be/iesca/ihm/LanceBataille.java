package be.iesca.ihm;

public class LanceBataille extends Thread {
	private String[] nomRobots;
	 private String sourcePath ;
	public LanceBataille(String[] nomRobots, String sourcePath) {
		super();
		this.nomRobots = nomRobots;
		this.sourcePath = sourcePath;
	}
	public String[] getNomRobots() {
		return nomRobots;
	}
	public void setNomRobots(String[] nomRobots) {
		this.nomRobots = nomRobots;
	}
	public String getSourcePath() {
		return sourcePath;
	}
	public void setSourcePath(String sourcePath) {
		this.sourcePath = sourcePath;
	}
	 
	 public  void run() {
		 LancementBataille lb = new LancementBataille(sourcePath, nomRobots);
		lb.demarrer();
		 
	 }

}
