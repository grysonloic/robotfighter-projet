package be.iesca.ihm;

import java.io.IOException;
import be.iesca.util.Compilateur;
import be.iesca.util.Fichier;
import robocode.control.BattleSpecification;
import robocode.control.BattlefieldSpecification;
import robocode.control.RobocodeEngine;
import robocode.control.RobotSpecification;

/**
 * Classe permettant de lancer une bataille. Les fichiers robots dont les noms
 * sont sp�cifi�s vont �tre copi�s et compil�s dans le r�pertoire  * /robots/robots/
 * Les fichiers .java et .class des robots seront dans ce
 * r�pertoire apr�s compilation. Ceci est n�cessaire pour que Robocode puisse
 * lancer la bataille. Quand Robocode d�marre, il lance la bataille avec les
 * robots pr�sents dans ce r�pertoire. Attention : le code source des robots
 * doit commencer par : package robots;
 */

public class LancementBataille {
	private String sourcePath;
	private String[] nomRobots;

	// ! /robots et non /robots/robots
	private String destPath = System.getProperty("user.dir") + "/robots";

	// car le compilateur g�n�re le "/robots" car "package robots;" dans le
	// fichier source;

	/*
	 * Constructeur
	 * 
	 * @param sourcePath : le r�pertoire contenant les sources java des robots.
	 * 
	 * @param nomRobots : noms des robots. Ex: {"Corners.java", "Crazy.java",
	 * "Fire.java"}
	 */
	public LancementBataille(String sourcePath, String[] nomRobots) {
		this.sourcePath = sourcePath;
		this.nomRobots = nomRobots;

		//-----------affichage console---------
		System.out.println("Creation lancement bataille..."); 


		try {
			// supprime les fichiers existants dans le r�pertoire utilis� par
			// Robocode
			Fichier.supprimerFichiers(this.destPath + "/robots");

			// copie les noms des fichiers .java dans le r�pertoire utilis� par
			// Robocode
			copierFichiers(sourcePath);

			// compile les fichiers. Les .class sont copi�s dans le r�pertoire
			// utilis� par Robocode
			compilerFichiers();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void copierFichiers(String sourcePath) {
		for (String nomRobot : this.nomRobots) {
			Fichier.copierFichier(nomRobot, sourcePath, this.destPath
					+ "/robots");
		}
	}

	/*
	 * M�thode permettant de lancer la bataille
	 */
	public void demarrer() {
		RobocodeEngine.setLogMessagesEnabled(false);
		RobocodeEngine engine = new RobocodeEngine();
		engine.setVisible(true);
		int numberOfRounds = 1;
		BattlefieldSpecification battlefield = new BattlefieldSpecification(
				800, 600);
		RobotSpecification[] selectedRobots = engine.getLocalRepository();
		BattleSpecification battleSpec = new BattleSpecification(
				numberOfRounds, battlefield, selectedRobots);
		engine.runBattle(battleSpec, true);
		engine.close();
		
		//-----------affichage console---------
		System.out.println("Bataille terminee.");
	}

	private void compilerFichiers() {
		String[] librairies = { "./libs/robocode.jar" };

		// Pour diriger la sortie vers un fichier et non la console
		boolean sortieConsole = false;
		
		Compilateur compilateur = new Compilateur(sourcePath, librairies,
				destPath, sortieConsole);
		compilateur.compiler();
	}

}
