package be.iesca.ihm;
/**
 *  Vue - Contrôleur principal
 */
import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import be.iesca.controleur.GestionnaireUseCases;
import be.iesca.domaine.Bundle;

@SuppressWarnings("serial")
public class VueControleurPrincipal extends JPanel implements ChangeListener{
	
	private JLabel JlTitre = new JLabel("Accueil", JLabel.CENTER);
	
	private JButton jbConnecter = new JButton("Déconnecter");
	private JButton jbModifier = new JButton("Compte");
	private JButton jbMBataille = new JButton("Bataille");
	private JButton jbMRobot = new JButton("Robot");
	
	private Bundle bundle = new Bundle();
	private Model model;
	private GestionnaireUseCases gestionnaire;
	private JFrame jframe;	

	public VueControleurPrincipal(Model model, JFrame jframe, Bundle bundle) {
		this.gestionnaire=GestionnaireUseCases.getInstance();
		this.jframe=jframe;
		this.model=model;
		this.model.addChangeListener(this);
		this.bundle = bundle;
		
		// bulle + clavier bouttons
		this.jbMBataille.setToolTipText("Acceder au menu des batailles (ALT+B)");
		this.jbMBataille.setMnemonic(KeyEvent.VK_B);
		
		this.jbModifier.setToolTipText("Modifier votre compte (ALT+C)");
		this.jbModifier.setMnemonic(KeyEvent.VK_C);
		
		this.jbMRobot.setToolTipText("Acceder au menu des robots (ALT+R)");
		this.jbMRobot.setMnemonic(KeyEvent.VK_R);
		
		this.jbConnecter.setToolTipText("Connectez-vous (ALT+C)");
		this.jbConnecter.setMnemonic(KeyEvent.VK_C);
		
		
		// ajouts des panels
		this.setLayout(new BorderLayout());
		this.add(creerPanelBoutonsAcceuil(), BorderLayout.CENTER);
		ajouterListenersBoutons();

		
	}

	protected void connecterDeconnecter() {
		 // déconnexion
			this.gestionnaire.deconnecterUser(bundle);
			if  ( (boolean) bundle.get(Bundle.OPERATION_REUSSIE) )
			{
				this.model.setBundle(bundle);
				this.jframe.remove(this);
				this.jframe.add(new FenetreConnexion(this.jframe, this.model));
				this.jframe.pack();
			}
		
		
	}

	protected void modifierCompte()
	{
		FenetreModification fm = new FenetreModification(this.jframe, bundle);
		fm.setVisible(true);
		if((boolean)fm.getBundle().get(Bundle.OPERATION_REUSSIE))
		{
			this.bundle=fm.getBundle();
		}
		this.model.setBundle(bundle);
	}
	
	private void menuBataille() {
		new MenuBataille(this);
		
	}
	
	private void menuRobot() {
		new MenuRobot(this);
		
	}

	private JPanel creerPanelBoutonsAcceuil()
	{
		this.JlTitre.setFont(new Font("Arial", Font.BOLD, 18));
		this.add(JlTitre, BorderLayout.NORTH);
		
		JPanel jpBoutons = new JPanel();
		
		jpBoutons.setLayout(new GridLayout(2, 2, 70, 30));
		
		jpBoutons.add(jbModifier);
		jpBoutons.add(jbMBataille);
		jpBoutons.add(jbMRobot);
		jpBoutons.add(jbConnecter);
		
		return jpBoutons;
	}
	
	private void ajouterListenersBoutons() {
		// listeners boutons
		jbConnecter.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				connecterDeconnecter();
			}
		});
		
		jbModifier.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				modifierCompte();
			}
		});
	
		jbMBataille.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				menuBataille();
			}

		});
		
		jbMRobot.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				menuRobot();
			}

		});
	}

	
	
	public Bundle getBundle()
	{
		return this.bundle;
	}
	
	public Model getModel()
	{
		return this.model;
	}
	
	public JFrame getJFrame()
	{
		return this.jframe;
	}

	@Override
	public void stateChanged(ChangeEvent arg0) {
		// TODO Auto-generated method stub
		
	}

}
