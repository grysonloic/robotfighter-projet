package be.iesca.tests;

import static org.testng.AssertJUnit.assertNotNull;
import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertFalse;
import static org.testng.AssertJUnit.assertTrue;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;import be.iesca.domaine.Bundle;
import be.iesca.domaine.User;
import be.iesca.usecase.GestionUsers;
import be.iesca.usecaseimpl.GestionUsersImpl;

/*
 * Tests integration du usecase GestionUsers
 * Ces tests supposent que la db contient 3 users (cf. script d'ajout)
 */
public class TestNG_GestionUsers {

	private static final String EMAIL_TOTO = "toto@gmail.com";
	private static final String NOM_TOTO = "Toto";
	private static final String PASSWORD_TOTO = "1234";
	private static final String AUTRE_PASSWORD = "****";
	private static final String EMAIL_TITI = "titi@gmail.com";
	private static final String EMAIL_TATA = "tata@gmail.com";
	private static final String NOM_TATA = "Tata";
	private static final String PASSWORD_TATA = "1234";

	private GestionUsers gestionUsers;
	private Bundle bundle;
	
	
	// initialisation du bundle et du gestionnaire de users fin de 
	// controle la bon fonctionnement entre les uses cases 
	// et le gestinnaire
 	@BeforeClass
	public void initialisation() {
		this.bundle = new Bundle();
		this.gestionUsers = new GestionUsersImpl();
	}
	
	// test si un user toto se connecte correctement
	@Test
	public void testConnecter1() {
		User user = new User();
		user.setEmail(EMAIL_TOTO);
		user.setPassword(PASSWORD_TOTO);
		bundle.put(Bundle.USER, user);
		this.gestionUsers.connecterUser(bundle);
		assertTrue((Boolean) bundle.get(Bundle.OPERATION_REUSSIE));
		user = (User) bundle.get(Bundle.USER);
		assertNotNull(user);
		assertEquals(NOM_TOTO, user.getNom());
		assertEquals(EMAIL_TOTO, user.getEmail());
	}
	
	// test de la connexion de toto avec un mauvais mot de passe
	// -> false mauvais mot de passe
	@Test
	public void testConnecter2() {
		User user = new User();
		user.setEmail(EMAIL_TOTO);
		user.setPassword(AUTRE_PASSWORD);
		bundle.put(Bundle.USER, user);
		this.gestionUsers.connecterUser(bundle);
		assertFalse((Boolean) bundle.get(Bundle.OPERATION_REUSSIE));
	}
	
	
	// test de connexion de user TATA
	// connexion ok
	@Test
	public void testConnecter3() {
		User user = new User();
		user.setEmail(EMAIL_TATA);
		user.setPassword(PASSWORD_TATA);
		bundle.put(Bundle.USER, user);
		this.gestionUsers.connecterUser(bundle);
		assertTrue((Boolean) bundle.get(Bundle.OPERATION_REUSSIE));
		user = (User) bundle.get(Bundle.USER);
		assertNotNull(user);
		assertEquals(NOM_TATA, user.getNom());
		assertEquals(EMAIL_TATA, user.getEmail());
	}
	
	// test connexion titi avec mot de passe de toto
	// -> false echec de la connexion
	@Test
	public void testConnecter4() {
		User user = new User();
		user.setEmail(EMAIL_TITI);
		user.setPassword(PASSWORD_TOTO);
		bundle.put(Bundle.USER, user);
		this.gestionUsers.connecterUser(bundle);
		assertFalse((Boolean) bundle.get(Bundle.OPERATION_REUSSIE));
	}

}