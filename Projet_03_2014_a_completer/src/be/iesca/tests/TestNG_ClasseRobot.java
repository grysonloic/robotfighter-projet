package be.iesca.tests;

import static org.testng.AssertJUnit.*;
import org.testng.annotations.Test;

import be.iesca.domaine.ClasseRobot;
import be.iesca.domaine.ExeptionsPerso;

public class TestNG_ClasseRobot {

	// Test Robot Classique compilable tout ok
	// -> true robot ok 
	@Test
	public void ajoutRobotOK() {

		boolean reussite = true;

		try {
			new ClasseRobot("Crazy1.java", ".\robots\test-ClassRobot");
		} catch (ExeptionsPerso e) {
			// TODO Auto-generated catch block
			reussite = false;
		} catch (Exception e) {
			reussite = false;
		} finally {
			assertTrue(reussite);
		}

	}

	// test d'un robot avec erreur compilation
	// -> false car erreur compil 
	@Test
	public void ajoutRobotNonCompilable() {

		boolean reussite = false;

		try {
			new ClasseRobot("Crazy4.java", ".\robots\test-ClassRobot");
		} catch (ExeptionsPerso e) {
			// TODO Auto-generated catch block
			reussite = true;
		} finally {
			assertFalse(reussite);
		}

	}

	// test de la pr�sence du package robot
	// false car package robot absent
	@Test
	public void ajoutAutrePackage() {

		boolean reussite = false;

		try {
			new ClasseRobot("Crazy2.java", ".\robots\test-ClassRobot");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			reussite = true;
		} finally {
			assertFalse(reussite);
		}

	}

	// test si le robot herite bien de Robot
	// -> false car robot test� n'herite pas de robot
	@Test
	public void ajoutRobotNonHerite() {

		boolean reussite = false;

		try {
			new ClasseRobot("Crazy3.java", ".\robots\test-ClassRobot");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			reussite = true;
		} finally {
			assertFalse(reussite);
		}

	}
}
