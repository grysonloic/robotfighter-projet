package be.iesca.tests;

import static org.testng.AssertJUnit.*;

import java.util.ArrayList;
import java.util.List;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import be.iesca.daoimpl.RobotDaoImpl;
import be.iesca.domaine.ClasseRobot;
import be.iesca.domaine.User;

public class TestNG_RobotsDaoImpl {

	private User user = new User();
	private ClasseRobot robot1 = new ClasseRobot();
	private ClasseRobot robot2 = new ClasseRobot();
	private List<ClasseRobot> robots;
	private RobotDaoImpl dao = new RobotDaoImpl();

	// creation de robot pour un user toto
	@BeforeClass
	public void initialiserListeRobot() {
		robots = new ArrayList<ClasseRobot>();

		robot1.setNom("Crazy");
		robot2.setNom("Corner");
		robots.add(robot1);
		robots.add(robot2);

		user.setEmail("test@test.com");
		user.setPassword("test");
		user.setNom("toto");

	}

	// ajout des robots pour le user toto -> true si r�ussite
	@Test
	public void AjoutTest() {
		for (ClasseRobot r : robots) {
			assertTrue(dao.ajouterRobot(r, user));

		}

	}

	// si robots ajout�, on liste ceux ci (toto)
	// -> liste de robot �gale au nombre de robots plac�s a l'ajout
	@Test(dependsOnMethods = { "AjoutTest" })
	public void listerTest() {
		List<ClasseRobot> robotsObtenus = dao.listerRobot(user);
		for (int i = 0; i < robots.size(); i++) {
			assertEquals(robotsObtenus.get(i), robots.get(i));
			robots.get(i).setId(robotsObtenus.get(i).getId());
		}

	}

	// suppression des robots de toto a la suite les uns des autres
	// -> r�ussie si les robots de toto sont tous supprim�s (un a un)
	@Test(dependsOnMethods = { "listerTest" })
	public void supprimerTest() {
		for (ClasseRobot r : robots) {
			assertTrue(dao.supprimerRobot(r.getId()));
		}
		List<ClasseRobot> robotsObtenus = dao.listerRobot(user);
		assertNotNull(robotsObtenus);
		assertEquals(0, robotsObtenus.size());

	}

	// vide la liste complete de robots en une seule fois
	// -> true si la suppresion est r�alis�e avec succ�s.
	@Test(dependsOnMethods = { "supprimerTest" })
	public void viderTest() {
		// vidage des robots de toto;

		robots.add(robot1);

		robots.add(robot2);

		for (ClasseRobot r : robots) {
			dao.ajouterRobot(r, user);

		}

		assertTrue(dao.vider(user));

	}

}
