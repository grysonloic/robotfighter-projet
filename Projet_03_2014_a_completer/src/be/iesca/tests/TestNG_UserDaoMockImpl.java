//Demoulin S�bastien
package be.iesca.tests;



import java.util.ArrayList;
import java.util.List;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import be.iesca.dao.UserDao;
import be.iesca.daoimpl.UserDaoMockImpl;
import be.iesca.domaine.User;
import static org.testng.AssertJUnit.*;

public class TestNG_UserDaoMockImpl {

	private static final String EMAIL_TOTO = "toto@gmail.com";
	private static final String NOM_TOTO = "Toto";
	private static final String PASSWORD_TOTO = "1234";
	private static final String AUTRE_PASSWORD = "****";
	private static final String EMAIL_TITI = "titi@gmail.com";
	private static final String EMAIL_TATA = "tata@gmail.com";
	private static final String NOM_TATA = "Tata";
	private static final String PASSWORD_TATA = "1234";
	protected UserDao userDao;
	private List<User> users;

	@BeforeClass
	// sera ex�cut� avant toutes les m�thodes
	public void initialiserLeDao() {
		userDao = new UserDaoMockImpl();
	}
	@BeforeClass
	// sera ex�cut� avant toutes les m�thodes
	public void initialiserListeBieres() {
		users = new ArrayList<User>(6);
		// ajout des bi�res (liste d�j� tri�e)
		users.add(new User("lol@gmail.com","lol","passlol"));
		users.add(new User("lol1@gmail.com","lol1","passlol1"));
		users.add(new User("lol2@gmail.com","lol2","passlol2"));
		users.add(new User("lol3@gmail.com","lol3","passlol3"));
		users.add(new User("lol4@gmail.com","lol4","passlol4"));
		users.add(new User("lol5@gmail.com","lol5","passlol5"));
		
	}
	
	
	@Test
	// on set un user et on regarde si c est bien ce qu"on a mit 
	// qui se trouve dedans
	public void test1() {
		User user = new User(EMAIL_TOTO,NOM_TOTO, PASSWORD_TOTO);
		assertNotNull(user);
		assertEquals(EMAIL_TOTO, user.getEmail());
		assertEquals(NOM_TOTO, user.getNom());
	}
	
	//recherche d'un user avec un mauvais mot de passe
	@Test(dependsOnMethods = { "test1" })
	public void test2() {
		User user = this.userDao.getUser(EMAIL_TOTO, AUTRE_PASSWORD,NOM_TOTO);
		assertNull(user);
	}
	
	// on set un user et on regarde si c est bien ce qu"on a mit 
	// qui se trouve dedans
	@Test(dependsOnMethods = { "test2" })
	public void test3() {
		User user = new User(EMAIL_TATA,NOM_TATA, PASSWORD_TATA);
		assertNotNull(user);
		assertEquals(EMAIL_TATA, user.getEmail());
		assertEquals(NOM_TATA, user.getNom());
	}
	
	// tentative de connexion de titi avec un mauvais mot de passe
	@Test(dependsOnMethods = { "test3" })
	public void test4() {
		User user = this.userDao.getUser(EMAIL_TITI, PASSWORD_TOTO,NOM_TOTO);
		assertNull(user);
	}
	
	//inscription du user valide
	@Test(dependsOnMethods = { "test4" })
	public void testInscrire() {
		for (User u : users) {
			assertTrue(userDao.inscrireUser(u));
		}
		
	}
	
	// tentative de modification du user
	@Test(dependsOnMethods = { "testInscrire" })
	public void testModifier() {
		User user = userDao.getUser(users.get(1).getEmail(), users.get(1).getPassword(),users.get(1).getNom());
		user.setEmail(user.getEmail()+"!");
		user.setNom(user.getNom()+"!");
		user.setPassword(user.getPassword()+"!");
		
		assertTrue(userDao.modifierUser(user));
		User userObtenue = userDao.getUser(user.getEmail(), user.getPassword(),user.getNom());
		assertEquals(userObtenue.getEmail(), user.getEmail());
	}
	
	
	
}
