package be.iesca.controleur;

import be.iesca.domaine.Bundle;
import be.iesca.domaine.User;
import be.iesca.usecase.GestionBatailles;
import be.iesca.usecase.GestionRobots;
import be.iesca.usecase.GestionUsers;
import be.iesca.usecaseimpl.GestionBataillesImpl;
import be.iesca.usecaseimpl.GestionRobotsImpl;
import be.iesca.usecaseimpl.GestionUsersImpl;

public class GestionnaireUseCases implements GestionUsers, GestionRobots , GestionBatailles {

	private static final GestionnaireUseCases INSTANCE = new GestionnaireUseCases();
	
	private User user; // null si pas identifi� par le syst�me
	private GestionUsersImpl gestionUsers;
	private GestionRobotsImpl gestionRobots;
	private GestionBataillesImpl gestionBatailles;
	
	public static GestionnaireUseCases getInstance() {
		return INSTANCE;
	}

	private GestionnaireUseCases() {
		this.gestionUsers = new GestionUsersImpl();
		this.gestionRobots = new GestionRobotsImpl();
		this.gestionBatailles= new GestionBataillesImpl();
		this.user = null; // pas de user connect�
	}

	@Override
	public void connecterUser(Bundle bundle) 
	{
		if (user == null) 
		{
			this.gestionUsers.connecterUser(bundle);
			if ((boolean) bundle.get(Bundle.OPERATION_REUSSIE)) 
			{
				this.user = (User) bundle.get(Bundle.USER);
			}
		}
		else 
		{ // un utilisateur est d�j� connect�
			bundle.put(Bundle.MESSAGE,
					"Op�ration impossible. Un utilisateur est d�j� connect�.");
			bundle.put(Bundle.OPERATION_REUSSIE, false);
		}
	}

	public void deconnecterUser(Bundle bundle) 
	{
		if (user == null) 
		{ // pas de user identifi�
			bundle.put(Bundle.MESSAGE,
					"Op�ration impossible. Pas d'utilisateur connect�.");
			bundle.put(Bundle.OPERATION_REUSSIE, false);
		}
		else 
		{
			this.user = null; // utilisateur d�connect�
			bundle.put(Bundle.MESSAGE, "Vous avez �t� d�connect�.");
			bundle.put(Bundle.OPERATION_REUSSIE, true);
		}
	}
	
	public void inscrireUser(Bundle bundle){
		this.gestionUsers.inscrireUser(bundle);
		
	}
	
public void modifierUser(Bundle bundle) {
		
		if (user == null) { // pas de user identifi�
			bundle.put(Bundle.MESSAGE,
					"Op�ration impossible. Pas d'utilisateur connect�.");
			bundle.put(Bundle.OPERATION_REUSSIE, false);
		} else {
			this.gestionUsers.modifierUser(bundle);
		}
	}

@Override
public void ajouterRobot(Bundle bundle) {
	// TODO Auto-generated method stub
	if (user == null) { // pas de user identifi�
		bundle.put(Bundle.MESSAGE,
				"Op�ration impossible. Pas d'utilisateur connect�.");
		bundle.put(Bundle.OPERATION_REUSSIE, false);
	} else {
		this.gestionRobots.ajouterRobot(bundle);
	}
	
}

@Override
public void supprimerRobot(Bundle bundle) {
	if (user == null) { // pas de user identifi�
		bundle.put(Bundle.MESSAGE,
				"Op�ration impossible. Pas d'utilisateur connect�.");
		bundle.put(Bundle.OPERATION_REUSSIE, false);
	} else {
		this.gestionRobots.supprimerRobot(bundle);
	}
	
}

@Override
public void vider(Bundle bundle) {
	// TODO Auto-generated method stub
	if (user == null) { // pas de user identifi�
		bundle.put(Bundle.MESSAGE,
				"Op�ration impossible. Pas d'utilisateur connect�.");
		bundle.put(Bundle.OPERATION_REUSSIE, false);
	} else {
		this.gestionRobots.vider(bundle);
	}
	

	
}

@Override
public void listerRobot(Bundle bundle) {
	if (user == null) {
		bundle.put(Bundle.MESSAGE, "Op�ration impossible. Pas d'utilisateur connect�.");
		bundle.put(Bundle.OPERATION_REUSSIE, false);
	}else {
		this.gestionRobots.listerRobot(bundle);
	}
	
}

//Demoulin S�bastien
@Override
public void listerRobotAnciennesBatailles(Bundle bundle) {
	// TODO Auto-generated method stub
	if (user == null) {
		bundle.put(Bundle.MESSAGE, "Op�ration impossible. Pas d'utilisateur connect�.");
		bundle.put(Bundle.OPERATION_REUSSIE, false);
	}else {
		this.gestionRobots.listerRobotAnciennesBatailles(bundle);
	}
	
}

@Override
public void listerBatailles(Bundle bundle) 
{
	if (user == null) {
		bundle.put(Bundle.MESSAGE, "Op�ration impossible. Pas d'utilisateur connect�.");
		bundle.put(Bundle.OPERATION_REUSSIE, false);
	}else {
		this.gestionBatailles.listerBatailles(bundle);
	}
	
}

@Override
public void getBataille(Bundle bundle) {

	if (user == null) {
		bundle.put(Bundle.MESSAGE, "Op�ration impossible. Pas d'utilisateur connect�.");
		bundle.put(Bundle.OPERATION_REUSSIE, false);
	}else {
		this.gestionBatailles.getBataille(bundle);
	}
	
}

@Override
public void supprimerRobotBataille(Bundle bundle) {
	if (user == null) { // pas de user identifi�
		bundle.put(Bundle.MESSAGE,
				"Op�ration impossible. Pas d'utilisateur connect�.");
		bundle.put(Bundle.OPERATION_REUSSIE, false);
	} else {
		this.gestionBatailles.supprimerRobotBataille(bundle);
	}
	
}


public void listerRobotBataille(Bundle bundle)
{
	if(user == null)
	{
		bundle.put(Bundle.MESSAGE,"Op�ration impossible. Pas d'utilisateur connect�.");
		bundle.put(Bundle.OPERATION_REUSSIE, false);
	}
	else
	{
		this.gestionRobots.listerRobotBataille(bundle);
	}
}

@Override
public boolean creerBataille(Bundle bundle)
{
	boolean ajoutReussi = false;
	if (user == null)
	{ // pas de user identifi�
		bundle.put(Bundle.MESSAGE,
				"Op�ration impossible. Pas d'utilisateur connect�.");
		bundle.put(Bundle.OPERATION_REUSSIE, false);
	}

	else
	{
		ajoutReussi = this.gestionBatailles.creerBataille(bundle);
	}
	return ajoutReussi;
}

public void ajouterRobotBataille(Bundle bundle) 
{
	// TODO Auto-generated method stub
	if(user==null)
	{
		bundle.put(Bundle.MESSAGE, "Op�ration impossible. Pas d'utilisateur connect�.");
		bundle.put(Bundle.OPERATION_REUSSIE,false);
	}
	else
	{
		this.gestionBatailles.ajouterRobotBataille(bundle);
	}
}

@Override
public void lancementBataille(Bundle bundle) {
	
	if(user==null)
	{
		bundle.put(Bundle.MESSAGE, "Op�ration impossible. Pas d'utilisateur connect�.");
		bundle.put(Bundle.OPERATION_REUSSIE,false);
	}
	else
	{
		this.gestionRobots.lancementBataille(bundle);
	}
	
}

public void supprimerBataille(Bundle bundle) 
{
	if(user==null)
	{
		bundle.put(Bundle.MESSAGE, "Op�ration impossible. Pas d'utilisateur connect�.");
		bundle.put(Bundle.OPERATION_REUSSIE,false);
	}
	else
	{
		this.gestionBatailles.supprimerBataille(bundle);
	}
	
}


}
