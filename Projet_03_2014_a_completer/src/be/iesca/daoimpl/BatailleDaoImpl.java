package be.iesca.daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;














import be.iesca.dao.BatailleDao;
import be.iesca.domaine.Bundle;
import be.iesca.domaine.User;


public class BatailleDaoImpl implements BatailleDao{
	private static final String LISTER = "select nom from batailles where pseudo_user = ?";
	private static final String SupprimerBatTabAssoc = "delete from robots_batailles where id_batailles= ?";
	private static final String SupprimerBat = "delete from batailles where id = ?";
	private static final String AJOUTROBBAT = "insert into robots_batailles (id_robots,id_batailles) values (?,?)";
	private static final String CREER = "insert into batailles (nom, pseudo_user) values (?,?)";
	private static final String Get = "select id from batailles where pseudo_user = ? and nom = ?";
	private static final String SUPPRIMER = "delete from robots_batailles where id_robots = ? and id_batailles= ?";
	
	private void cloturer(ResultSet rs, PreparedStatement ps, Connection con) {
		try {
			if (rs != null)
				rs.close();
		} catch (Exception ex) {
		}
		try {
			if (ps != null)
				ps.close();
		} catch (Exception ex) {
		}
		try {
			if (con != null)
				con.close();
		} catch (Exception ex) {
		}
	}

	@Override
	public List<String> listerBataille(User user) {
		List<String> liste = new ArrayList<String>();
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = DaoFactory.getInstance().getConnexion();
			ps = con.prepareStatement(LISTER);
			ps.setString(1, user.getNom().trim());
			rs= ps.executeQuery();
			String nom = "";
			while(rs.next())
			{
				nom = rs.getString("nom");
				liste.add(nom);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			cloturer(null, ps, con);
		}
		return liste;
	}

	public boolean creerBataille(Bundle bundle, User user)
	{
		boolean ajout = false;
	
		Connection con = null;
		PreparedStatement ps = null;
		
		try {
			con = DaoFactory.getInstance().getConnexion();
			ps = con.prepareStatement(CREER);
			
			ps.setString(1,(String)bundle.get(Bundle.BATAILLE));
			ps.setString(2, user.getNom());
			
			int resultat = ps.executeUpdate();
			
			
			if (resultat == 1)
			{
				ajout = true;
			}
		} 
		catch (Exception ex) 
		{
			ex.printStackTrace();
		} 
		finally 
		{
			cloturer(null, ps, con);
		}
		
		return ajout;
	}
	
	@Override
	public int getBataille(Bundle bundle) {
		
		int id = 0;
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		User user = (User) bundle.get(Bundle.USER);
		try {
			con = DaoFactory.getInstance().getConnexion();
			ps = con.prepareStatement(Get);
			ps.setString(1, user.getNom().trim());
			ps.setString(2, (String) bundle.get(Bundle.BATAILLE));
			rs= ps.executeQuery();
			 
			while(rs.next())
			{
				id = rs.getInt("id");
			
			}
			
			
			
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			cloturer(null, ps, con);
		}
		return id;
	}

	@SuppressWarnings("unused")
	@Override
	public boolean supprimerRobotBataille(String id, Bundle bundle) {
		boolean suppressionReussie = false;
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		int nombre = Integer.parseInt(id);
		try {
			con = DaoFactory.getInstance().getConnexion();
			ps = con.prepareStatement(SUPPRIMER);
			//ps = con.prepareStatement(SUPPRIMER);
			ps.setInt(1, nombre);
			ps.setInt(2,(int) bundle.get(Bundle.IDBATAILLE));
			int nb = ps.executeUpdate();
			if (nb == 1) suppressionReussie = true;
				
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			cloturer(rs, ps, con);
		}
		return true;
	}

	@Override
	public boolean ajouterRobotBataille(String id, Bundle bundle) 
	{
		// TODO Auto-generated method stub
		
		boolean ajoutReussi = false;
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		int nombre = Integer.parseInt(id);
		try {
			con = DaoFactory.getInstance().getConnexion();
			ps = con.prepareStatement(AJOUTROBBAT);
			ps.setInt(1, nombre);
			ps.setInt(2,(int) bundle.get(Bundle.IDBATAILLE));
			
			int nb = ps.executeUpdate();
			if (nb == 1) ajoutReussi = true;
				
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			cloturer(rs, ps, con);
		}
		return ajoutReussi;

	}

	@Override
	public boolean supprimerBataille(Bundle bundle) 
	{
		boolean suppressionReussie = false;
		Connection con = null;
		PreparedStatement ps = null;
		PreparedStatement ps1 = null;

		ResultSet rs = null;
		try {
			con = DaoFactory.getInstance().getConnexion();
			
			ps = con.prepareStatement(SupprimerBatTabAssoc);
			ps.setInt(1,(int) bundle.get(Bundle.IDBATAILLE));
			int nb1 = ps.executeUpdate();
			
			ps1 = con.prepareStatement(SupprimerBat);
			ps1.setInt(1,(int) bundle.get(Bundle.IDBATAILLE));
			int nb2 = ps1.executeUpdate();			
			
			if ( nb1 !=0 && nb2 != 0) suppressionReussie = true;
				
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			cloturer(rs, ps, con);
		}
		return suppressionReussie;
	}

	



}
