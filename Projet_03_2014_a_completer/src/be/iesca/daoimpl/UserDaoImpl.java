package be.iesca.daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import be.iesca.dao.UserDao;
import be.iesca.domaine.User;

public class UserDaoImpl implements UserDao {
	private static final String GET = "SELECT * FROM users WHERE email=? and password = crypt(?, password) ";
	private static final String VERIFAJOUT= "SELECT * FROM users WHERE email=? and nom=? ";
	private static final String AJOUT = "INSERT INTO users (nom, email,password) VALUES (?,?,crypt(?, gen_salt('bf',8)))";
	private static final String MAJ = "UPDATE users SET  password=crypt(?, gen_salt('bf',8)) where nom= ?";

	// obligatoire pour pouvoir construire une instance avec newInstance() 
	public UserDaoImpl() {
	}

	@Override
	public User getUser(String email, String password,String pseudo) {
		User user = null;
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = DaoFactory.getInstance().getConnexion();
			ps = con.prepareStatement(GET);
			ps.setString(1, email.trim());
			ps.setString(2, password.trim());
			
			rs = ps.executeQuery();
			if (rs.next()) {
				user = new User();
				user.setNom(rs.getString("nom"));
				user.setEmail(rs.getString("email"));
				user.setPassword(rs.getString("password"));
				
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			cloturer(rs, ps, con);
		}
		return user;
	}

	private void cloturer(ResultSet rs, PreparedStatement ps, Connection con) {
		try {
			if (rs != null)
				rs.close();
		} catch (Exception ex) {
		}
		try {
			if (ps != null)
				ps.close();
		} catch (Exception ex) {
		}
		try {
			if (con != null)
				con.close();
		} catch (Exception ex) {
		}
	}

	@Override
	public Boolean inscrireUser(User user) {
		boolean ajoutReussi = false;
		Connection con = null;
		PreparedStatement ps = null;
		
		try {
			
			con = DaoFactory.getInstance().getConnexion();
			ps = con.prepareStatement(AJOUT);
			ps.setString(1, user.getNom().trim());
			ps.setString(2, user.getEmail().trim());
			ps.setString(3, user.getPassword().trim());
			
			int resultat = ps.executeUpdate();
			if (resultat == 1) {
				ajoutReussi = true;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			cloturer(null, ps, con);
		}
		
		
			
		return ajoutReussi;
	}

	@Override
	public Boolean modifierUser(User user) {
		boolean modificationReussie = false;
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = DaoFactory.getInstance().getConnexion();
			ps = con.prepareStatement(MAJ);
			String nom = user.getNom().trim();
			ps.setString(2, nom); // <------------------- 4 �me param�tre !
			ps.setString(1, user.getPassword().trim());
			
			
			int resultat = ps.executeUpdate();
			if (resultat == 1) {
				modificationReussie = true;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			cloturer(null, ps, con);
		}
		return modificationReussie;
	}

	@Override
	public User getUserVerif(String email, String pseudo) {
		User user = null;
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = DaoFactory.getInstance().getConnexion();
			ps = con.prepareStatement(VERIFAJOUT);
			ps.setString(1, email.trim());
			ps.setString(2, pseudo.trim());
			
			rs = ps.executeQuery();
			if (rs.next()) {
				user = new User();
				user.setNom(rs.getString("nom"));
				user.setEmail(rs.getString("email"));
				
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			cloturer(rs, ps, con);
		}
		return user;
	}
}
