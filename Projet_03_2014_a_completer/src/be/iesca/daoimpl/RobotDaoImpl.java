package be.iesca.daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import be.iesca.dao.RobotDao;
import be.iesca.domaine.ClasseRobot;
import be.iesca.domaine.User;
import be.iesca.util.Fichier;

public class RobotDaoImpl implements RobotDao {
	
	private static final String VIDER = "delete from robots where pseudo_user = ?";
	private static final String LISTERROBBAT = "select nom, id from robots";
	private static final String AJOUT = "insert into robots (nom,code,pseudo_user) values (?,?,?)";
	private static final String LISTER = "select nom, id from robots where pseudo_user = ? order by id";
	private static final String SUPPRIMER = "delete from robots where id = ?";
	private static final String LISTERANCIENNES = "select robots.nom, robots.id  from robots,  robots_batailles where robots_batailles.id_batailles= ? and robots_batailles.id_robots=robots.id";
	private static final String ROBOTBATAILLE = "SELECT nom, code FROM Robots r WHERE r.nom = ? and r.id= ?";
	
	
	private void cloturer(ResultSet rs, PreparedStatement ps, Connection con) {
		try {
			if (rs != null)
				rs.close();
		} catch (Exception ex) {
		}
		try {
			if (ps != null)
				ps.close();
		} catch (Exception ex) {
		}
		try {
			if (con != null)
				con.close();
		} catch (Exception ex) {
		}
	}


	@Override
	public boolean supprimerRobot(String robot) {
		boolean suppressionReussie = false;
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		int nombre = Integer.parseInt(robot);
		try {
			con = DaoFactory.getInstance().getConnexion();
			ps = con.prepareStatement(SUPPRIMER);
			//ps = con.prepareStatement(SUPPRIMER);
			ps.setInt(1, nombre);
			int nb = ps.executeUpdate();
			if (nb == 1) suppressionReussie = true;
				
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			cloturer(rs, ps, con);
		}
		return suppressionReussie;
	}

	@Override
	public List<ClasseRobot> listerRobot(User user) {
		List<ClasseRobot> liste = new ArrayList<ClasseRobot>();
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = DaoFactory.getInstance().getConnexion();
			ps = con.prepareStatement(LISTER);
			ps.setString(1, user.getNom().trim());
			rs= ps.executeQuery();
			String nom = "";
			while(rs.next())
			{
				nom = rs.getString("nom");
				ClasseRobot robot = new ClasseRobot(nom, rs.getString(2));
				robot.setId(rs.getString("id"));
				liste.add(robot);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			cloturer(null, ps, con);
		}
		return liste;
	}

	@Override
	public boolean vider(User user) {
		
		boolean vider = false;
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = DaoFactory.getInstance().getConnexion();
			ps = con.prepareStatement(VIDER);
			ps.setString(1, user.getNom().trim());
			
			int resultat = ps.executeUpdate();
			
			if (resultat > 0) {
				vider = true;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			cloturer(null, ps, con);
		}
		return vider;
	}

	@Override
	public boolean ajouterRobot(ClasseRobot robot, User user) {
		
		boolean ajout = false;
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = DaoFactory.getInstance().getConnexion();
			ps = con.prepareStatement(AJOUT);
			ps.setString(1, robot.getNom().trim());
			ps.setString(2, robot.getCode().trim());
			ps.setString(3, user.getNom().trim());
			
			int resultat = ps.executeUpdate();
			if (resultat == 1) {
				ajout = true;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			cloturer(null, ps, con);
		}
		return ajout;
	}


	@Override
	public List<ClasseRobot> listerRobotAnciennesBatailles(User user,
			String nomBatailles, int idBataille) {
		
		List<ClasseRobot> liste = new ArrayList<ClasseRobot>();
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = DaoFactory.getInstance().getConnexion();
			ps = con.prepareStatement(LISTERANCIENNES);
			//a changer
			ps.setInt(1, idBataille);
			rs= ps.executeQuery();
			String nom = "";
			while(rs.next())
			{
				nom = rs.getString("nom");
				ClasseRobot robot = new ClasseRobot(nom, rs.getString(2));
				robot.setId(rs.getString("id"));
				liste.add(robot);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			cloturer(null, ps, con);
		}
		return liste;
	}


	@Override
	public List<ClasseRobot> listerRobotBataille() 
	{
		List<ClasseRobot> liste = new ArrayList<ClasseRobot>();
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = DaoFactory.getInstance().getConnexion();
			ps = con.prepareStatement(LISTERROBBAT);
		
			rs= ps.executeQuery();
			String nom = "";
			while(rs.next())
			{
				nom = rs.getString("nom");
				ClasseRobot robot = new ClasseRobot(nom, rs.getString(2));
				robot.setId(rs.getString("id"));
				liste.add(robot);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			cloturer(null, ps, con);
		}
		return liste;
	}


	@SuppressWarnings("static-access")
	@Override
	public String RobotLancementBataille(ClasseRobot robot) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String nom="";
		try {
			Fichier fich=new Fichier();
			String destPath = "./robots/test-robots/";
			
			con = DaoFactory.getInstance().getConnexion();
			ps = con.prepareStatement(ROBOTBATAILLE);
			ps.setString(1,robot.getNom());
			ps.setInt(2,Integer.parseInt(robot.getId()));
			rs= ps.executeQuery();
			
			
			while(rs.next())
			{
				
				nom = rs.getString("nom")+".java";
				
				fich.creerFichier(rs, "code", destPath ,nom );	
			}
			
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			cloturer(null, ps, con);
		}
		return nom;
	}

}
