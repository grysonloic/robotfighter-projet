package be.iesca.daoimpl;

import java.util.Comparator;
import java.util.Map;
//Demoulin Sébastien
import java.util.TreeMap;

import be.iesca.dao.UserDao;
import be.iesca.domaine.User;

public class UserDaoMockImpl implements UserDao{
	private  Map<String,User> mapUsers;
	
	
	public UserDaoMockImpl()
	{
		Comparator<String> comp = new ComparateurUsers();
		this.mapUsers = new TreeMap<String, User>(comp);
	}

	public Map<String, User> getMapUsers() {
		return mapUsers;
	}

	public void setMapUsers(Map<String, User> mapUsers) {
		this.mapUsers = mapUsers;
	}

	@Override
	public User getUser(String email, String password,String pseudo)
	{
		try {
				User user =this.mapUsers.get(email);
				if ( user==null) return null;
				return new User(user);
		} catch (Exception ex) {
			return null;
		}
	}
	
	
	private class ComparateurUsers implements Comparator<String> {
		@Override
		public int compare(String nom1, String nom2) {
			return nom1.compareTo(nom2);
		}
	}


	@Override
	public Boolean inscrireUser(User user) {
		// TODO Auto-generated method stub
		try {
			if (this.mapUsers.containsKey(user.getEmail()))
				return false;
			this.mapUsers.put(user.getEmail(), user);
		} catch (Exception ex) {
			return false;
		}
		
		return true;
	}

	@Override
	public Boolean modifierUser(User user) {
		// TODO Auto-generated method stub
		try {
			if (this.mapUsers.containsKey(user.getEmail()))
				return false;
			this.mapUsers.put(user.getEmail(), user);
		
		} catch (Exception ex) {
			return false;
		}
		return true;
	}

	@Override
	public User getUserVerif(String email, String pseudo) {
		try {
			User user =this.mapUsers.get(email);
			if ( user==null) return null;
			return new User(user);
	} catch (Exception ex) {
		return null;
	}
	}
}
