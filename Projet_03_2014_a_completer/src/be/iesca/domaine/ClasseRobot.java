package be.iesca.domaine;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.swing.JOptionPane;

import robocode.Robot;
import be.iesca.util.Compilateur;
import be.iesca.util.Fichier;

public class ClasseRobot {
	public static final String PACKAGE_ROBOTS = "robots";
	private String nom; // nom simple de la classe. Ex: "Fire" (de
						// "robots.Fire.java")
	private String id;

	private String code = "";

	public String getCode() {
		return code;
	}

	public ClasseRobot() {
		super();
	}

	@SuppressWarnings("static-access")
	public ClasseRobot(String nom, String path) throws ExeptionsPerso /*
																 * throws
																 * Exception
																 */{
		super();
		this.nom = nom;
		File fichier = new File(path);
		Fichier deplacement;
		String[] librairies = { "./libs/robocode.jar" };
		Compilateur compil = new Compilateur("./src/robots", librairies,
				"./src/robots/", true);
		byte[] buffer = new byte[1];

		// v�rification de la pr�sence du fichier
		if (fichier.exists()) {

			deplacement = new Fichier();
			path = path.replace(nom, "");
			try {
				deplacement.supprimerFichiers("./src/robots/");
			} catch (IOException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
			deplacement.copierFichier(nom, path, "./src/robots");
			// v�rification compilation

			compil.compiler();
			if (compil.compilationReussie()) {

				// Chargement de la classe
				// � placer apr�s la compilation (sinon ClassNotFoundException)
				// v�rification du package

				Class<?> cl = null;
				try {
					String classe = nom.replace(".java", "");
					cl = Class.forName(PACKAGE_ROBOTS + "." + classe);
				} catch (ClassNotFoundException e1) {

				} catch (NullPointerException e) {
				}

				try {
					cl.asSubclass(Robot.class);
				} catch (ClassCastException e) {
					JOptionPane.showMessageDialog(null, "Mauvais fichier");
					System.out.println("2");
				} catch (NullPointerException e) {
				}

				FileInputStream fichComplil = null;
				try {
					fichComplil = new FileInputStream(new File(
							"./src/robots/" + nom));
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					JOptionPane.showMessageDialog(null, "Mauvais fichier");
				}

				try {
					while (fichComplil.read(buffer) != -1) {

						code += new String(buffer);

					}

				} catch (IOException e) {
					// TODO Auto-generated catch block
					JOptionPane.showMessageDialog(null, "Mauvais fichier");
				}

				// )

			} else {
				throw (new ExeptionsPerso());
			}

		}

	}
	
	public ClasseRobot(ClasseRobot robot) {
		super();
		this.nom = robot.nom;
	}
	
	public void supprimerRobot(String nom, String path) throws IOException
	{
		nom+=".java";
		File fichier = new File(path+"\\"+nom);
		if (fichier.exists()) {Fichier.supprimerUnFichier(nom, path);JOptionPane.showMessageDialog(null, "Fichier supprimer avec succ�s");}
		else JOptionPane.showMessageDialog(null, "Fichier inexistant");
		
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Robot [nom=" + nom + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nom == null) ? 0 : nom.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ClasseRobot other = (ClasseRobot) obj;
		if (nom == null) {
			if (other.nom != null)
				return false;
		} else if (!nom.equals(other.nom))
			return false;
		return true;
	}
}

