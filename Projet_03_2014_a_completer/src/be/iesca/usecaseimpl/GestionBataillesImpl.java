package be.iesca.usecaseimpl;


import java.util.List;

import be.iesca.dao.BatailleDao;
import be.iesca.daoimpl.DaoFactory;
import be.iesca.domaine.Bundle;
import be.iesca.domaine.ClasseRobot;
import be.iesca.domaine.User;
import be.iesca.usecase.GestionBatailles;



public class GestionBataillesImpl implements GestionBatailles{

	private BatailleDao batailleDao;

	public GestionBataillesImpl() {
		this.batailleDao = (BatailleDao) DaoFactory.getInstance().getDaoImpl(
				BatailleDao.class);
		// pour mock
		// this.robotDao= DaoFactory.getInstance().getRobotDao();
	}
	@Override
	public void listerBatailles(Bundle bundle) {
		
			boolean listeOk = true;
			String message = "";
			User user = (User) bundle.get(Bundle.USER);

			List<String> listeBataille = null;
			listeBataille = this.batailleDao.listerBataille(user);

			if (listeBataille == null) {
				listeOk = false;
			} else if (listeBataille.isEmpty()){
				listeOk = false;
				message = "Vous n'avez pas de batailles";}
			else if (listeBataille.size() == 1)
				message = "Il y a 1 bataille";
			else
				message = "Il y a " + listeBataille.size() + " batailles";
			bundle.put(Bundle.OPERATION_REUSSIE, listeOk);
			bundle.put(Bundle.MESSAGE, message);
			bundle.put(Bundle.LISTEBATAILLE, listeBataille);
		
	}
	
	public boolean creerBataille(Bundle bundle)
	{
		boolean ajoutReussi = false;
		String message="";
		boolean nomExistant= false;

		User user = (User) bundle.get(Bundle.USER);
		List<String> listeBataille = null;
		listeBataille = this.batailleDao.listerBataille(user);
		
		if(listeBataille.contains(bundle.get(Bundle.BATAILLE)))
		{
			nomExistant = true;
		}

		if(nomExistant == false)
		{
			ajoutReussi = this.batailleDao.creerBataille(bundle,user);
	
			if(ajoutReussi == false)
				message = "La cr�ation de la bataille a �chou�";
			else
				message = "La cr�ation de la bataille a r�ussi";
		}
		else
			message = "Nom de bataille d�j� utilis�";
		
		bundle.put(Bundle.OPERATION_REUSSIE, ajoutReussi);
		bundle.put(Bundle.MESSAGE, message);
		
		return ajoutReussi;
	}
	
	@Override
	public void getBataille(Bundle bundle) {
		// TODO Auto-generated method stub
		
		
		int id = 0 ;
		
		id = this.batailleDao.getBataille( bundle);
		
		if(id!=0)
		{	
			bundle.put(Bundle.OPERATION_REUSSIE, true);
			bundle.put(Bundle.MESSAGE, "Bataille trouv�e");
			bundle.put(Bundle.IDBATAILLE, id);
			
		
			
		}
		else
		{
			bundle.put(Bundle.OPERATION_REUSSIE, false);
			bundle.put(Bundle.MESSAGE, "Bataille non trouv�e");
		}
		
	}
	@Override
	public void supprimerRobotBataille(Bundle bundle) {
		ClasseRobot robot = (ClasseRobot) bundle.get(Bundle.ROBOT);
		String message = "";
		boolean suppressionReussie = false;
		suppressionReussie = this.batailleDao.supprimerRobotBataille(robot.getId(),bundle);
		if (suppressionReussie)
			message = "Suppresion r�alis�e avec succ�s";
		else
			message = "Echec suppression";
		bundle.put(Bundle.OPERATION_REUSSIE, suppressionReussie);
		bundle.put(Bundle.MESSAGE, message);
		
	}
	public void ajouterRobotBataille(Bundle bundle) 
	{
		// TODO Auto-generated method stub
		ClasseRobot robot = (ClasseRobot) bundle.get(Bundle.ROBOT);
		String message = "";
		boolean ajoutReussi = false;
		ajoutReussi = this.batailleDao.ajouterRobotBataille(robot.getId(), bundle);
		if (ajoutReussi)
			message = "Cr��e avec succ�s";
		else
			message = "Echec de la cr�ation";
		bundle.put(Bundle.OPERATION_REUSSIE, ajoutReussi);
		bundle.put(Bundle.MESSAGE, message);
	}
	public void supprimerBataille(Bundle bundle) 
	{
		// TODO Auto-generated method stub
		boolean suppressionReussie = false;
		String message = "";
		suppressionReussie = this.batailleDao.supprimerBataille(bundle);
		
		if(suppressionReussie)
			message = "suppression r�ussie avec succ�s";
		else
			message = "�chec suppression";
		
		bundle.put(Bundle.OPERATION_REUSSIE, suppressionReussie);
		bundle.put(Bundle.MESSAGE, message);
	}
	

}
