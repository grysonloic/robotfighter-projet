package be.iesca.usecaseimpl;

import java.util.List;

import javax.swing.JOptionPane;

import be.iesca.dao.RobotDao;
import be.iesca.daoimpl.DaoFactory;
import be.iesca.domaine.Bundle;
import be.iesca.domaine.ClasseRobot;
import be.iesca.domaine.User;
import be.iesca.usecase.GestionRobots;

public class GestionRobotsImpl implements GestionRobots {

	private RobotDao robotDao;

	public GestionRobotsImpl() {
		this.robotDao = (RobotDao) DaoFactory.getInstance().getDaoImpl(
				RobotDao.class);
		// pour mock
		// this.robotDao= DaoFactory.getInstance().getRobotDao();
	}

	@Override
	public void ajouterRobot(Bundle bundle) {
		ClasseRobot robot = (ClasseRobot) bundle.get(Bundle.ROBOT);
		User user = (User) bundle.get(Bundle.USER);
		String message = "";
		boolean ajoutOK = false;
		ajoutOK = this.robotDao.ajouterRobot(robot, user);
		if (ajoutOK)
			message = "Robot ajout� avec succ�s";
		else
			message = "Echec de l'ajout";
		bundle.put(Bundle.OPERATION_REUSSIE, ajoutOK);
		bundle.put(Bundle.MESSAGE, message);

	}

	@Override
	public void supprimerRobot(Bundle bundle) {
		ClasseRobot robot = (ClasseRobot) bundle.get(Bundle.ROBOT);
		String message = "";
		boolean suppressionReussie = false;
		suppressionReussie = this.robotDao.supprimerRobot(robot.getId());
		if (suppressionReussie)
			message = "Suppresion r�alis�e avec succ�s";
		else
			message = "Echec suppression";
		bundle.put(Bundle.OPERATION_REUSSIE, suppressionReussie);
		bundle.put(Bundle.MESSAGE, message);

	}

	@Override
	public void vider(Bundle bundle) {
		// TODO Auto-generated method stub
		String message;
		User user = (User) bundle.get(Bundle.USER);
		boolean suppressionReussie = false;
		suppressionReussie = this.robotDao.vider(user);
		if (suppressionReussie)
			message = "Suppresion r�alis�e avec succ�s";
		else
			message = "Rien � supprimer";
		
		bundle.put(Bundle.OPERATION_REUSSIE, suppressionReussie);
		bundle.put(Bundle.MESSAGE, message);
		JOptionPane.showMessageDialog(null, bundle.get(Bundle.MESSAGE),"Information", JOptionPane.INFORMATION_MESSAGE);
	}

	@Override
	public void listerRobot(Bundle bundle) {
		boolean listeOk = true;
		String message = "";
		User user = (User) bundle.get(Bundle.USER);

		List<ClasseRobot> listeRobot = null;
		listeRobot = this.robotDao.listerRobot(user);

		if (listeRobot == null) {
			listeOk = false;
		} else if (listeRobot.isEmpty()){
			listeOk = false;
			message = "Vous n'avez pas de robots";}
		else if (listeRobot.size() == 1)
			message = "Il y a 1 Robot";
		else
			message = "Il y a " + listeRobot.size() + " robots";
		bundle.put(Bundle.OPERATION_REUSSIE, listeOk);
		bundle.put(Bundle.MESSAGE, message);
		bundle.put(Bundle.LISTE, listeRobot);

	}
	//Demoulin S�bastien
	@Override
	public void listerRobotAnciennesBatailles(Bundle bundle) {
		boolean listeOk = true;
		String message = "";
		User user = (User) bundle.get(Bundle.USER);
		String nomBatailles = (String) bundle.get(Bundle.BATAILLE);

		List<ClasseRobot> listeRobot = null;
		if(bundle.get(Bundle.IDBATAILLE) != null)
		{
			int idBataille = (int) bundle.get(Bundle.IDBATAILLE);
		listeRobot = this.robotDao.listerRobotAnciennesBatailles(user , nomBatailles , idBataille);

		if (listeRobot == null) {
			listeOk = false;
		} else if (listeRobot.isEmpty()){
			listeOk = false;
			message = "Vous n'avez pas de robots";}
		else if (listeRobot.size() == 1)
			message = "Il y a 1 Robot";
		else
			message = "Il y a " + listeRobot.size() + " robots";
		}
		else
		{
			listeOk=false;
			message = "Vous n'avez pas de robots";
		}
		bundle.put(Bundle.OPERATION_REUSSIE, listeOk);
		bundle.put(Bundle.MESSAGE, message);
		bundle.put(Bundle.LISTE, listeRobot);
		
	}

	public void listerRobotBataille(Bundle bundle) 
	{
		// TODO Auto-generated method stub
		boolean listeOk = true;
		String message = "";
		@SuppressWarnings("unused")
		User user = (User) bundle.get(Bundle.USER);

		List<ClasseRobot> listeRobot = null;
		listeRobot = this.robotDao.listerRobotBataille();

		if (listeRobot == null) {
			listeOk = false;
		} else if (listeRobot.isEmpty()){
			listeOk = false;
			message = "Vous n'avez pas de robots";}
		else if (listeRobot.size() == 1)
			message = "Il y a 1 Robot";
		else
			message = "Il y a " + listeRobot.size() + " robots";
		bundle.put(Bundle.OPERATION_REUSSIE, listeOk);
		bundle.put(Bundle.MESSAGE, message);
		bundle.put(Bundle.LISTE, listeRobot);
	}

	public void lancementBataille(Bundle bundle) {
		boolean listeOk = true;
		ClasseRobot robot = (ClasseRobot) bundle.get(Bundle.ROBOT);
		String listeRobotBataille = null;
		listeRobotBataille = this.robotDao.RobotLancementBataille(robot);

		if (listeRobotBataille == null) 
			listeOk = false;
		else
			bundle.put(Bundle.NOMROBOTJAVA, listeRobotBataille);
		
		bundle.put(Bundle.OPERATION_REUSSIE, listeOk);
		bundle.put(Bundle.LISTE, listeRobotBataille);
		
	}

	

	

	
	


}
