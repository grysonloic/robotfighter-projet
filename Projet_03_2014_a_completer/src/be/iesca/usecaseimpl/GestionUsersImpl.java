package be.iesca.usecaseimpl;

import be.iesca.dao.UserDao;
import be.iesca.daoimpl.DaoFactory;
import be.iesca.domaine.Bundle;
import be.iesca.domaine.User;
import be.iesca.usecase.GestionUsers;

public class GestionUsersImpl implements GestionUsers {

	private UserDao userDao;

	public GestionUsersImpl() {
		this.userDao = (UserDao) DaoFactory.getInstance().getDaoImpl(
				UserDao.class);
		// pour mock
		// this.userDao= DaoFactory.getInstance().getUserDao();
	}

	@Override
	public void connecterUser(Bundle bundle) {
		User userDB = null;
		boolean operationReussie = false;
		String message = "";
		User user = (User) bundle.get(Bundle.USER);
		if (user == null) {
			message = "Pas d'utilisateur!";
		} else {
			// S'il appuie sur connecter sans remplir les champs
			if (user.getEmail().isEmpty() || user.getPassword().isEmpty()) {
				message += "Champs incomplets.";
			} else {
				String email = user.getEmail();
				String password = user.getPassword();
				String pseudo = user.getNom();
				
				userDB = this.userDao.getUser(email, password, pseudo);
				if (userDB == null) {
					message = "Echec lors de l'identification.";
				} else {
					message = "Bonjour " + userDB.getNom()
							+ ". Vous �tes connect�.";
					operationReussie = true;
					bundle.put(Bundle.USER, userDB);
				}
			}
		}
		bundle.put(Bundle.OPERATION_REUSSIE, operationReussie);
		bundle.put(Bundle.MESSAGE, message);
	}

	@Override
	public void inscrireUser(Bundle bundle) {
		// TODO Auto-generated method stub

		Boolean ajoutReussi = false;
		String message = "";

		User user = (User) bundle.get(Bundle.USER);

		

		if (user.getEmail() == "" || user.getEmail().isEmpty()
				|| user.getNom() == "" || user.getEmail().isEmpty()
				|| user.getPassword() == "" || user.getPassword().isEmpty()) {
			message += "Champ(s) incomplet(s) \n";
		}

		else {

			User userDb = this.userDao.getUserVerif(user.getEmail(),
					 user.getNom());
			
			if (userDb == null
					&& (user.getEmail().length() != 0
							|| user.getNom().length() != 0 || user
							.getPassword().length() != 0)) {
				
				ajoutReussi = this.userDao.inscrireUser(user);

				if (ajoutReussi)
					message += "Vous avez bien �t� inscrit";
				else {
					message += "L'utilisateur n'a pu etre inscrit ";
				}

			} else {
				message += "Utilisateur d�j� existant \n";

				try {

					if (userDb.getEmail() != null) {

						message += "E-mail existant, veuillez en choisir un autre \n";
					}
				} catch (NullPointerException e) {
				}

				try {
					if (userDb.getNom() != null) {
						message += "Pseudo deja existant \n";
					}
				} catch (NullPointerException e) {
				}
			}

		}

		bundle.put(Bundle.OPERATION_REUSSIE, ajoutReussi);
		bundle.put(Bundle.USER, user);
		bundle.put(Bundle.MESSAGE, message);
	}

	@Override
	public void modifierUser(Bundle bundle) {
		boolean operationReussie = false;
		String message = "";
		User user = (User) bundle.get(Bundle.USER);
		String Newligne=System.getProperty("line.separator"); 

		String ExpressionReguliere = "(^[a-zA-Z0-9._%+-]*)";

		if (user.getPassword().isEmpty() || user.getPassword()=="" ) {
			message += "Champ Password vide"+Newligne;
		} 
		if (!user.getPassword().matches(ExpressionReguliere)  || user.getPassword().isEmpty() || user.getPassword()=="")
		{
			message +="Le password entr� est incorrecte"+ Newligne;
		}
		else 
		{
			operationReussie = this.userDao.modifierUser(user);
			if (operationReussie) {
				message = "Modification r�alis�e avec succ�s";
				bundle.put(Bundle.USER, user);
			} else {
				message += "La modification n'a pas pu �tre r�alis�e";
			}
		}
		bundle.put(Bundle.OPERATION_REUSSIE, operationReussie);
		bundle.put(Bundle.MESSAGE, message);
	}
}
